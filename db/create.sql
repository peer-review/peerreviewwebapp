-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.6.4-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дъмп на структурата на БД peer-review
CREATE DATABASE IF NOT EXISTS `peer-review` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `peer-review`;

-- Дъмп структура за таблица peer-review.audit_log
CREATE TABLE IF NOT EXISTS `audit_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log` text NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `creator_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `comments_users_user_id_fk` (`creator_id`),
  KEY `comments_work_items_item_id_fk` (`item_id`),
  CONSTRAINT `comments_users_user_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `comments_work_items_item_id_fk` FOREIGN KEY (`item_id`) REFERENCES `work_items` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.item_version
CREATE TABLE IF NOT EXISTS `item_version` (
  `item_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  KEY `item_version_versions_version_id_fk` (`version_id`),
  KEY `item_version_work_items_item_id_fk` (`item_id`),
  CONSTRAINT `item_version_versions_version_id_fk` FOREIGN KEY (`version_id`) REFERENCES `versions` (`version_id`),
  CONSTRAINT `item_version_work_items_item_id_fk` FOREIGN KEY (`item_id`) REFERENCES `work_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.reviewers_items
CREATE TABLE IF NOT EXISTS `reviewers_items` (
  `reviewer_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`reviewer_item_id`),
  KEY `reviewers_items_users_user_id_fk` (`reviewer_id`),
  KEY `reviewers_items_work_items_item_id_fk` (`item_id`),
  KEY `reviewers_items_statuses_status_id_fk` (`status_id`),
  CONSTRAINT `reviewers_items_statuses_status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`status_id`),
  CONSTRAINT `reviewers_items_users_user_id_fk` FOREIGN KEY (`reviewer_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `reviewers_items_work_items_item_id_fk` FOREIGN KEY (`item_id`) REFERENCES `work_items` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `roles_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`status_id`),
  UNIQUE KEY `statuses_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.teams
CREATE TABLE IF NOT EXISTS `teams` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `minimum_reviewers` int(11) NOT NULL DEFAULT 1,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`team_id`),
  UNIQUE KEY `teams_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.team_members
CREATE TABLE IF NOT EXISTS `team_members` (
  `team_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  KEY `team_members_teams_team_id_fk` (`team_id`),
  KEY `team_members_users_user_id_fk` (`member_id`),
  CONSTRAINT `team_members_teams_team_id_fk` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`),
  CONSTRAINT `team_members_users_user_id_fk` FOREIGN KEY (`member_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.team_work_items
CREATE TABLE IF NOT EXISTS `team_work_items` (
  `team_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  UNIQUE KEY `team_work_items_item_id_uindex` (`item_id`),
  KEY `team_work_items_work_items_item_id_fk` (`team_id`),
  CONSTRAINT `team_work_items_teams_team_id_fk` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`),
  CONSTRAINT `team_work_items_work_items_item_id_fk` FOREIGN KEY (`item_id`) REFERENCES `work_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_num` varchar(10) NOT NULL,
  `user_photo` varchar(1000) NOT NULL,
  `enabled` tinyint(1) DEFAULT 0,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  UNIQUE KEY `users_phone_num_uindex` (`phone_num`),
  UNIQUE KEY `users_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.users_roles
CREATE TABLE IF NOT EXISTS `users_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `users_roles_roles_role_id_fk` (`role_id`),
  KEY `users_roles_users_user_id_fk` (`user_id`),
  CONSTRAINT `users_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `users_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.verification_token
CREATE TABLE IF NOT EXISTS `verification_token` (
  `token_id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `expire_date` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`token_id`),
  KEY `verification_token__users_fk` (`user_id`),
  CONSTRAINT `verification_token__users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.versions
CREATE TABLE IF NOT EXISTS `versions` (
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `title` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `creator_id` int(11) NOT NULL,
  `upload` datetime NOT NULL DEFAULT current_timestamp(),
  `last_update` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица peer-review.work_items
CREATE TABLE IF NOT EXISTS `work_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `item_status` int(11) DEFAULT 1,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
