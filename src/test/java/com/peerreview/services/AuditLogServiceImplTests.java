package com.peerreview.services;

import com.peerreview.repositories.contracts.AuditLogRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class AuditLogServiceImplTests {
    @Mock
    AuditLogRepository mockRepository;

    @InjectMocks
    AuditLogServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void create_should_callRepository() {
        service.create("log");

        Mockito.verify(mockRepository, Mockito.times(1))
                .create("log");
    }

}
