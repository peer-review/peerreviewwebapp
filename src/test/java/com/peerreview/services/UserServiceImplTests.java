package com.peerreview.services;


import com.peerreview.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

//    @Test
//    public void register_should_callRepository_when_UserWithSameIdDoesNotExist() {
//        User mockUser = Helpers.createMockUser();
//
//        Mockito.when(userRepository.getUserByUsername(mockUser.getUsername()))
//                .thenThrow(EntityNotFoundException.class);
//
//        userService.register(mockUser);
//
//        Mockito.verify(userRepository, Mockito.times(1))
//                .create(mockUser);
//    }
}
