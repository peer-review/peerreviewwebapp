package com.peerreview.services;

import com.peerreview.Helpers;
import com.peerreview.models.Comment;
import com.peerreview.models.WorkItem;
import com.peerreview.repositories.contracts.CommentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CommentServiceImplTests {
    @Mock
    CommentRepository mockRepository;

    @InjectMocks
    CommentServiceImpl service;

    @Test
    void addCommentToTask_should_callRepository() {

        service.addCommentToTask(Helpers.createMockItem(), Helpers.createMockComment("Comment", Helpers.createMockUser()));

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(Helpers.createMockComment("Comment", Helpers.createMockUser()));
    }

    @Test
    void addCommentToTask_should_addCommentInItem() {
        WorkItem workItem = Helpers.createMockItem();
        Comment comment = Helpers.createMockComment("Comment", Helpers.createMockUser());
        service.addCommentToTask(workItem, comment);
        Comment commentToEquals = workItem.getComments().stream().findAny().get();
        Assertions.assertEquals(commentToEquals, comment);
    }

    @Test
    public void getById_should_returnComment_when_matchExist() {
        Comment mockComment = Helpers.createMockComment("comment", Helpers.createMockUser());
        Mockito.when(mockRepository.getById(mockComment.getId()))
                .thenReturn(mockComment);

        Comment result = service.getById(mockComment.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockComment.getId(), result.getId()),
                () -> Assertions.assertEquals(mockComment.getCommentCreator(), result.getCommentCreator()),
                () -> Assertions.assertEquals(mockComment.getComment(), result.getComment())
        );
    }

    @Test
    void create_should_callRepository() {
        WorkItem workItem = Helpers.createMockItem();
        Comment comment = Helpers.createMockComment("Comment", Helpers.createMockUser());
        service.create(comment);
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(comment);

    }

    @Test
    public void itemsComments_should_returnComments_when_matchExist() {
        Mockito.when(mockRepository.itemComments(1))
                .thenReturn(List.of());

        service.itemComments(1);

        Mockito.verify(mockRepository, Mockito.times(1))
                .itemComments(Mockito.anyInt());

    }
}
