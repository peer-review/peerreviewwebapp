package com.peerreview.services;

import com.peerreview.Helpers;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Version;
import com.peerreview.repositories.contracts.VersionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class VersionServiceImplTests {
    @Mock
    VersionRepository mockRepository;
    @InjectMocks
    VersionServiceImpl service;

    @Test
    public void create_should_callRepository_when_versionWithSameIdDoesNotExist() {
        Version mockVersion = Helpers.createMockVersion();

        Mockito.when(mockRepository.getById(mockVersion.getId()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockVersion, Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockVersion);
    }

    @Test
    public void create_should_throwException_when_versionExist() {
        Version mockVersion = Helpers.createMockVersion();

        Mockito.when(mockRepository.getById(mockVersion.getId()))
                .thenReturn(mockVersion);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockVersion, Helpers.createMockUser()));
    }

    @Test
    public void update_should_callRepository_when_versionWithSameIdDoesNotExist() {
        Version mockVersion = Helpers.createMockVersion();

        Mockito.when(mockRepository.getById(mockVersion.getId()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockVersion, Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockVersion);
    }

    @Test
    public void update_should_throwException_when_versionExist() {
        Version mockVersion = Helpers.createMockVersion();
        Version mockVersionUpdate = Helpers.createMockVersion();
        mockVersionUpdate.setId(10);
        Mockito.when(mockRepository.getById(mockVersion.getId()))
                .thenReturn(mockVersionUpdate);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockVersion, Helpers.createMockUser()));
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotCreator() {
        Version mockVersion = Helpers.createMockVersion();
        mockVersion.setCreator(Helpers.createMockAdmin());
        mockVersion.getCreator().setId(5);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockVersion);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockVersion.getId(), Helpers.createMockUser()));

    }

    @Test
    void delete_should_callRepository_when_initiatorIsNotCreator() {
        Version mockVersion = Helpers.createMockVersion();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockVersion);

        service.delete(mockVersion.getId(), Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockVersion.getId());

    }

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll(Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnVersion_when_matchExist() {
        Version mockVersion = Helpers.createMockVersion();

        Mockito.when(mockRepository.getById(mockVersion.getId()))
                .thenReturn(mockVersion);

        Version result = service.getById(mockVersion.getId(), Helpers.createMockAdmin());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockVersion.getId(), result.getId()),
                () -> Assertions.assertEquals(mockVersion.getCreator().getId(), result.getCreator().getId()),
                () -> Assertions.assertEquals(mockVersion.getStatus(), result.getStatus()));
    }

    @Test
    public void getByTitle_should_returnVersion_when_matchExist() {
        Version mockVersion = Helpers.createMockVersion();

        Mockito.when(mockRepository.getByTitle(Mockito.anyString()))
                .thenReturn(mockVersion);

        Version result = service.getByTitle(Mockito.anyString());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockVersion.getId(), result.getId()),
                () -> Assertions.assertEquals(mockVersion.getCreator().getId(), result.getCreator().getId()),
                () -> Assertions.assertEquals(mockVersion.getStatus(), result.getStatus()));
    }

    @Test
    public void createVersion_should_returnVersion_when_matchExist() {
        Mockito.when(mockRepository.getById(0))
                .thenThrow(EntityNotFoundException.class);

        service.createVersion(Helpers.createMockItem(), Helpers.createMockUser());
    }

    @Test
    void getAllVersionOfItem_should_callRepository() {
        Mockito.when(mockRepository.getAllVersionOfItem(Mockito.anyInt()))
                .thenReturn(new ArrayList<>());

        service.getAllVersionOfItem(Mockito.anyInt());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllVersionOfItem(Mockito.anyInt());
    }
}
