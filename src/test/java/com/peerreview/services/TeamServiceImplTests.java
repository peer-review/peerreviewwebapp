package com.peerreview.services;


import com.peerreview.Helpers;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Team;
import com.peerreview.models.User;
import com.peerreview.repositories.contracts.TeamRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class TeamServiceImplTests {

    @Mock
    TeamRepository teamRepository;

    @Mock
    UserServiceImpl userService;

    @InjectMocks
    TeamServiceImpl teamService;

    @Test
    public void create_should_callRepository_when_teamWithSameIdDoesNotExist() {
        Team mockTeam = Helpers.createMockTeam();

        Mockito.when(teamRepository.getByTeamName(mockTeam.getName()))
                .thenThrow(EntityNotFoundException.class);

        teamService.create(mockTeam, Helpers.createMockUser());

        Mockito.verify(teamRepository, Mockito.times(1))
                .create(mockTeam);
    }

    @Test
    public void create_should_throwException_when_teamExist() {
        Team mockTeam = Helpers.createMockTeam();

        Mockito.when(teamRepository.getByTeamName(Mockito.anyString()))
                .thenReturn(mockTeam);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> teamService.create(mockTeam, Helpers.createMockUser()));
    }

    @Test
    public void update_should_throwException_when_teamExist() {
        Team mockTeam = Helpers.createMockTeam();
        Team mockTeamUpdate = Helpers.createMockTeam();
        mockTeamUpdate.setId(2);
        Mockito.when(teamRepository.getById(mockTeam.getId()))
                .thenReturn(mockTeam);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> teamService.update(mockTeam, Helpers.createMockUser()));
    }

    @Test
    public void update_should_callRepository_when_itemWithSameIdDoesNotExist() {
        Team mockTeam = Helpers.createMockTeam();

        Mockito.when(teamRepository.getById(mockTeam.getId()))
                .thenThrow(EntityNotFoundException.class);

        teamService.update(mockTeam, Helpers.createMockUser());

        Mockito.verify(teamRepository, Mockito.times(1))
                .update(mockTeam);
    }

    @Test
    public void update_should_throwException_when_isNotUser() {
        Team mockTeam = Helpers.createMockTeam();
        User mockUser = Helpers.createMockUser();
        mockUser.setUsername("Nicola");
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> teamService.update(mockTeam, mockUser));
    }

    @Test
    public void update_should_throwException_when_isNotOwner() {
        Team mockTeam = Helpers.createMockTeam();
        User mockUser = Helpers.createMockUser();
        mockUser.setUsername("Nicola");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> teamService.update(mockTeam, mockUser));
    }

    @Test
    void getAll_should_callRepository() {
        Mockito.when(teamRepository.getAll())
                .thenReturn(new ArrayList<>());

        teamService.getAll(Helpers.createMockAdmin());

        Mockito.verify(teamRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getAll_should_throwException_when_isNotAdmin() {


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> teamService.getAll(Helpers.createMockUser()));
    }

    @Test
    public void getById_should_returnTeam_when_matchExist() {
        Team mockTeam = Helpers.createMockTeam();

        Mockito.when(teamRepository.getById(Mockito.anyInt()))
                .thenReturn(mockTeam);

        Team result = teamService.getById(Mockito.anyInt(), Helpers.createMockAdmin());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockTeam.getId(), result.getId()),
                () -> Assertions.assertEquals(mockTeam.getName(), result.getName()),
                () -> Assertions.assertEquals(mockTeam.getOwner().getUsername(), result.getOwner().getUsername()));

    }

    @Test
    public void addMemberToItem_should_callRepository() {
        Team mockTeam = Helpers.createMockTeam();


    }
    @Test
    public void addMemberToTeam_should_throwException_when_userNotOwner() {

    }
}
