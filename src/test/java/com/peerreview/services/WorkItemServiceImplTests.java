package com.peerreview.services;

import com.peerreview.Helpers;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Status;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.repositories.contracts.WorkItemRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class WorkItemServiceImplTests {
    @Mock
    WorkItemRepository mockRepository;
    @Mock
    AuditLogServiceImpl auditLogService;
    @Mock
    VersionServiceImpl versionService;
    @Mock
    StatusServiceImpl statusService;
    @Mock
    UserServiceImpl userService;
    @InjectMocks
    WorkItemServiceImpl service;

    @Test
    public void create_should_callRepository_when_itemWithSameIdDoesNotExist() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(mockRepository.getById(mockItem.getId()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockItem, Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockItem);
    }

    @Test
    public void create_should_throwException_when_itemExist() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockItem);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockItem, Helpers.createMockUser()));
    }

    @Test
    public void update_should_throwException_when_itemExist() {
        WorkItem mockItem = Helpers.createMockItem();
        WorkItem mockItemUpdate = Helpers.createMockItem();
        mockItemUpdate.setId(10);
        Mockito.when(mockRepository.getById(mockItem.getId()))
                .thenReturn(mockItemUpdate);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockItem, Helpers.createMockUser()));
    }

    @Test
    public void update_should_callRepository_when_itemWithSameIdDoesNotExist() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(mockRepository.getById(mockItem.getId()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockItem, Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockItem);
    }


    @Test
    public void update_should_throwException_when_userNotMember() {
        WorkItem mockItem = Helpers.createMockItem();
        mockItem.getCreator().setId(10);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockItem, Helpers.createMockUser()));
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotCreator() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockItem);

        mockItem.getCreator().setId(10);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockItem.getId(), Helpers.createMockUser()));
    }

    @Test
    void delete_should_callRepository_when_initiatorIsCreator() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockItem);

        service.delete(mockItem.getId(), Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockItem);
    }

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll(Helpers.createMockAdmin());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getAll_should_throwException_when_initiatorIsUser() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAll(Helpers.createMockUser()));
    }

    @Test
    public void getById_should_returnItem_when_matchExist() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockItem);

        WorkItem result = service.getById(mockItem.getId(), Helpers.createMockAdmin());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockItem.getId(), result.getId()),
                () -> Assertions.assertEquals(mockItem.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockItem.getDescription(), result.getDescription()),
                () -> Assertions.assertEquals(mockItem.getCreator().getId(), result.getCreator().getId()),
                () -> Assertions.assertEquals(mockItem.getStatus(), result.getStatus()));
    }

    @Test
    void getById_should_throwException_when_initiatorIsNotInWorkItem() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockItem);
        User initiator = Helpers.createMockUser();
        initiator.setId(10);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(Mockito.anyInt(), initiator));
    }

    @Test
    public void getByTitle_should_returnItem_when_matchExist() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(mockRepository.getByTitle(Mockito.anyString()))
                .thenReturn(mockItem);

        WorkItem result = service.getByTitle(Mockito.anyString(), Helpers.createMockAdmin());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockItem.getId(), result.getId()),
                () -> Assertions.assertEquals(mockItem.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockItem.getDescription(), result.getDescription()),
                () -> Assertions.assertEquals(mockItem.getCreator().getId(), result.getCreator().getId()),
                () -> Assertions.assertEquals(mockItem.getStatus(), result.getStatus()));
    }

    @Test
    void getByTitle_should_throwException_when_initiatorIsNotInWorkItem() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(mockRepository.getByTitle(Mockito.anyString()))
                .thenReturn(mockItem);
        User initiator = Helpers.createMockUser();
        initiator.setId(10);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByTitle(Mockito.anyString(), initiator));
    }

    @Test
    void search_should_callRepository() {
        service.search(Optional.of("string"), Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .search(Optional.of("string"));
    }

    @Test
    void search_should_callRepository_whenSearchIsEmpty() {
        service.search(Optional.empty(), Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void filter_should_callRepository() {
        service.filter(Optional.of("string"), Optional.of("string"), Optional.of("string"), Optional.of("string"), Optional.of("string"), Optional.of("string"), Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .filter(Optional.of("string"), Optional.of("string"), Optional.of("string"), Optional.of("string"), Optional.of("string"), Optional.of("string"));
    }

    @Test
    public void addReviewerToItem_should_throwException_when_userNotMember() {
        WorkItem mockItem = Helpers.createMockItem();
        mockItem.getCreator().setId(10);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.addReviewerToItem(mockItem, "username", Helpers.createMockUser()));
    }

    @Test
    public void addReviewerToItem_should_callRepository() {
        WorkItem mockItem = Helpers.createMockItem();

        Mockito.when(userService.getUserByUsername("username"))
                .thenReturn(Helpers.createMockUser());

        Mockito.when(mockRepository.getAllStatusesOfItem(1))
                .thenReturn(List.of(Helpers.createMockStatus("a")));

        service.addReviewerToItem(mockItem, "username", Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockItem);
    }

    @Test
    public void getFinalStatusOfAllReviewers_should_callRepository() {
        Mockito.when(mockRepository.getAllStatusesOfItem(1))
                .thenReturn(List.of());

        Mockito.when(statusService.getByName("Pending"))
                .thenReturn(Helpers.createMockStatus("Pending"));

        Status result = service.getFinalStatusOfAllReviewers(1);

        Assertions.assertEquals("Pending", result.getName());
    }

    @Test
    public void getFinalStatusOfAllReviewers_should_returnStatusRejected() {
        Mockito.when(mockRepository.getAllStatusesOfItem(1))
                .thenReturn(List.of(Helpers.createMockStatus("Pending"), Helpers.createMockStatus("Rejected")));

        Status result = service.getFinalStatusOfAllReviewers(1);

        Assertions.assertEquals("Rejected", result.getName());
    }

    @Test
    public void getFinalStatusOfAllReviewers_should_returnUnderReview() {
        Mockito.when(mockRepository.getAllStatusesOfItem(1))
                .thenReturn(List.of(Helpers.createMockStatus("Pending"), Helpers.createMockStatus("Under Review")));

        Status result = service.getFinalStatusOfAllReviewers(1);

        Assertions.assertEquals("Under Review", result.getName());
    }

    @Test
    public void getFinalStatusOfAllReviewers_should_returnChangeRequest() {
        Mockito.when(mockRepository.getAllStatusesOfItem(1))
                .thenReturn(List.of(Helpers.createMockStatus("Pending"), Helpers.createMockStatus("Change Requested")));

        Status result = service.getFinalStatusOfAllReviewers(1);

        Assertions.assertEquals("Change Requested", result.getName());
    }

    @Test
    public void getDrafts_should_callRepository() {

        service.getDrafts(Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getDrafts(Helpers.createMockUser());
    }

    @Test
    public void updateStatus_should_callRepository() {
        Mockito.when(mockRepository.getAllStatusesOfItem(1))
                .thenReturn(List.of());

        Mockito.when(statusService.getByName("Pending"))
                .thenReturn(Helpers.createMockStatus("Pending"));

        service.updateStatus(Helpers.createMockItem());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(Helpers.createMockItem());
    }
}
