package com.peerreview.services;

import com.peerreview.Helpers;
import com.peerreview.repositories.contracts.StatusRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class StatusServiceImplTests {
    @Mock
    StatusRepository mockRepository;

    @InjectMocks
    StatusServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll(Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void geById_should_callRepository() {
        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(Helpers.createMockStatus("Pending"));

        service.getById(Mockito.anyInt(), Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    void geByName_should_callRepository() {
        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(Helpers.createMockStatus("Pending"));

        service.getByName(Mockito.anyString());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByName(Mockito.anyString());
    }
}
