package com.peerreview.services;

import com.peerreview.Helpers;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Reviewer;
import com.peerreview.repositories.contracts.ReviewerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class ReviewerServiceImplTests {
    @Mock
    ReviewerRepository mockRepository;
    @Mock
    AuditLogServiceImpl auditLogService;
    @Mock
    WorkItemServiceImpl workItemService;
    @InjectMocks
    ReviewerServiceImpl service;


    @Test
    public void create_should_callRepository_when_reviewerWithSameIdDoesNotExist() {
        Reviewer mockReviewer = Helpers.createMockReviewer();

        Mockito.when(mockRepository.getByItemAndUsername(mockReviewer.getWorkItem().getId(), mockReviewer.getReviewer().getUsername()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockReviewer, Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockReviewer);
    }

    @Test
    public void create_should_throwException_when_reviewerNotMember() {
        Reviewer reviewer = Helpers.createMockReviewer();
        reviewer.setWorkItem(Helpers.createMockItem());
        reviewer.getWorkItem().setId(5);
        reviewer.getReviewer().setId(5);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(reviewer, reviewer.getReviewer()));
    }

    @Test
    public void create_should_throwException_when_reviewerExist() {
        Reviewer mockReviewer = Helpers.createMockReviewer();

        Mockito.when(mockRepository.getByItemAndUsername(mockReviewer.getWorkItem().getId(), mockReviewer.getReviewer().getUsername()))
                .thenReturn(mockReviewer);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockReviewer, Helpers.createMockUser()));
    }

    @Test
    public void update_should_throwException_when_reviewerExist() {
        Reviewer mockReviewer = Helpers.createMockReviewer();
        Reviewer mockReviewerUpdate = Helpers.createMockReviewer();
        mockReviewerUpdate.getReviewer().setId(10);
        Mockito.when(mockRepository.getByItemAndUsername(mockReviewer.getWorkItem().getId(), mockReviewer.getReviewer().getUsername()))
                .thenReturn(mockReviewer);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockReviewerUpdate, Helpers.createMockUser()));
    }

    @Test
    public void update_should_callRepository_when_reviewerWithSameIdDoesNotExist2() {
        Reviewer mockReviewer = Helpers.createMockReviewer();

        Mockito.when(mockRepository.getByItemAndUsername(mockReviewer.getWorkItem().getId(), mockReviewer.getReviewer().getUsername()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockReviewer, Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockReviewer);
    }

    @Test
    public void update_should_callRepository_when_reviewerWithSameIdDoesNotExist() {
        Reviewer mockReviewer = Helpers.createMockReviewer();

        Mockito.when(mockRepository.getByItemAndUsername(mockReviewer.getWorkItem().getId(), mockReviewer.getReviewer().getUsername()))
                .thenReturn(mockReviewer);

        service.update(mockReviewer, Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockReviewer);
    }

    @Test
    public void update_should_throwException_when_reviewerNotMember() {
        Reviewer reviewer = Helpers.createMockReviewer();
        reviewer.setWorkItem(Helpers.createMockItem());
        reviewer.getWorkItem().setId(5);
        reviewer.getWorkItem().getReviewers().add(Helpers.createMockUser());
        reviewer.getReviewer().setId(5);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(reviewer, reviewer.getReviewer()));
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotCreator() {
        Reviewer mockReviewer = Helpers.createMockReviewer();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockReviewer);

        mockReviewer.getWorkItem().getCreator().setId(10);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockReviewer.getId(), Helpers.createMockUser()));

    }

    @Test
    void delete_should_callRepository_when_initiatorIsCreator() {
        Reviewer mockReviewer = Helpers.createMockReviewer();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockReviewer);

        service.delete(mockReviewer.getId(), Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockReviewer.getId());
    }

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll(Helpers.createMockUser());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnReviewer_when_matchExist() {
        Reviewer mockReviewer = Helpers.createMockReviewer();
        Mockito.when(mockRepository.getById(mockReviewer.getId()))
                .thenReturn(mockReviewer);

        Reviewer result = service.getById(mockReviewer.getId(), Helpers.createMockAdmin());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockReviewer.getId(), result.getId()),
                () -> Assertions.assertEquals(mockReviewer.getReviewer().getId(), result.getReviewer().getId()),
                () -> Assertions.assertEquals(mockReviewer.getWorkItem(), result.getWorkItem()));
    }

    @Test
    public void getByItemAndUsername_should_returnReviewer_when_matchExist() {
        Reviewer mockReviewer = Helpers.createMockReviewer();
        Mockito.when(mockRepository.getByItemAndUsername(mockReviewer.getWorkItem().getId(), mockReviewer.getReviewer().getUsername()))
                .thenReturn(mockReviewer);

        Reviewer result = service.getByItemAndUsername(mockReviewer.getId(), mockReviewer.getReviewer().getUsername());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockReviewer.getId(), result.getId()),
                () -> Assertions.assertEquals(mockReviewer.getReviewer().getId(), result.getReviewer().getId()),
                () -> Assertions.assertEquals(mockReviewer.getWorkItem(), result.getWorkItem()));
    }
}