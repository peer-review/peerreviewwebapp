package com.peerreview;

import com.peerreview.models.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class Helpers {
    public static User createMockUser() {
        return createMockUser("User");
    }

    public static User createMockAdmin() {
        return createMockUser("Admin");
    }

    private static User createMockUser(String role) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setUsername("mockUsername");
        mockUser.setPassword("mockUsername");

        mockUser.setEmail("mockMail@mail.com");
        mockUser.setPhoneNumber("1234567890");
        mockUser.setRole(Set.of(createMockRole(role)));
        return mockUser;
    }

    public static Role createMockRole(String role) {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setName(role);
        return mockRole;
    }

    public static WorkItem createMockItem() {
        var mockItem = new WorkItem();
        mockItem.setId(1);
        mockItem.setTitle("Mock Title");
        mockItem.setDescription("Mock Description");
        mockItem.setCreator(createMockUser());
        mockItem.setStatus(createMockStatus("Pending"));
        mockItem.setDeleted(false);
        mockItem.setComments(new HashSet<>());
        mockItem.setReviewers(new HashSet<>());
        return mockItem;
    }

    public static Status createMockStatus(String name) {
        var mockStatus = new Status();
        mockStatus.setId(1);
        mockStatus.setName(name);
        return mockStatus;
    }

    public static Comment createMockComment(String comment, User user) {
        var mockComment = new Comment();
        mockComment.setId(1);
        mockComment.setCommentCreator(user);
        mockComment.setComment(comment);
        return mockComment;
    }

    public static Reviewer createMockReviewer() {
        var mockReviewer = new Reviewer();
        mockReviewer.setId(1);
        mockReviewer.setReviewer(createMockUser());
        mockReviewer.setStatus(createMockStatus("Pending"));
        mockReviewer.setWorkItem(createMockItem());
        return mockReviewer;
    }

    public static Version createMockVersion() {
        var mockVersion = new Version();
        mockVersion.setId(1);
        mockVersion.setItemId(1);
        mockVersion.setItemTitle("Title");
        mockVersion.setItemDescription("Description");
        mockVersion.setCreator(createMockUser());
        mockVersion.setUpload(LocalDateTime.now());
        mockVersion.setLastUpdate(LocalDateTime.now().minusHours(1));
        mockVersion.setStatus(createMockStatus("Pending"));
        return mockVersion;
    }

    public static Team createMockTeam() {
        var mockTeam = new Team();

        mockTeam.setId(1);
        mockTeam.setName("Olympic");
        mockTeam.setOwner(createMockUser());
        mockTeam.setDeleted(false);
        mockTeam.setMinimumReviewers(1);
        mockTeam.setWorkItem(new HashSet<>());
        mockTeam.setMembers(new HashSet<>());
        return mockTeam;
    }
}
