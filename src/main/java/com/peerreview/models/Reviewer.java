package com.peerreview.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "reviewers_items")
public class Reviewer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reviewer_item_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "item_id")
    private WorkItem workItem;

    @ManyToOne
    @JoinColumn(name = "reviewer_id")
    private User reviewer;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    public Reviewer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public WorkItem getWorkItem() {
        return workItem;
    }

    public void setWorkItem(WorkItem workItem) {
        this.workItem = workItem;
    }

    public User getReviewer() {
        return reviewer;
    }

    public void setReviewer(User reviewer) {
        this.reviewer = reviewer;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reviewer reviewer1 = (Reviewer) o;
        return id == reviewer1.id && Objects.equals(workItem, reviewer1.workItem) && Objects.equals(reviewer, reviewer1.reviewer) && Objects.equals(status, reviewer1.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, workItem, reviewer, status);
    }
}
