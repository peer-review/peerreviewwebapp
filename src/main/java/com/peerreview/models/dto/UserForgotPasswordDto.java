package com.peerreview.models.dto;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserForgotPasswordDto {

    private String username;


    @Size(min = 8)
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
            message = "Password must be at least 8 symbols and should " +
            "contain capital letter, digit, and special symbol (+, -, *, &, ^, …)")
    private String newPassword;

    @Size(min = 8)
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
            message = "Confirm password must be at least 8 symbols and should " +
                    "contain capital letter, digit, and special symbol (+, -, *, &, ^, …)")
    private String confirmPassword;


    public UserForgotPasswordDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
