package com.peerreview.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class TeamDto {

    @NotNull(message = "Team name should not be null!")
    @Size(min = 3, max = 30)
    private String name;

    @Positive
    private int minimumReviewer;

    public TeamDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinimumReviewer() {
        return minimumReviewer;
    }

    public void setMinimumReviewer(int minimumReviewer) {
        this.minimumReviewer = minimumReviewer;
    }
}
