package com.peerreview.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class InvitationDto {

    @NotNull(message = "The email should not be null!")

    private String email;

    private String subject;

    private String text;


    public InvitationDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
