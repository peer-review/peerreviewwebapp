package com.peerreview.models.dto;

public class WorkItemDtoMvc extends WorkItemDtoBase {

    private String[] reviewers = new String[1000];

    public WorkItemDtoMvc() {
    }

    public String[] getReviewers() {
        return reviewers;
    }

    public void setReviewers(String[] reviewers) {
        this.reviewers = reviewers;
    }
}
