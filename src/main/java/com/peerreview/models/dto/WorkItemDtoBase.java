package com.peerreview.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class WorkItemDtoBase {
    @NotBlank(message = "Title can't be blank")
    @Size(min = 10, max = 80, message = "Item title should be between 10 and 80 symbols")
    private String title;

    @NotBlank(message = "Description can't be blank")
    @Size(min = 20, message = "Description name should be over 20 symbols")
    private String description;

    public WorkItemDtoBase() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
