package com.peerreview.models.dto;

import javax.validation.constraints.NotNull;

public class MemberDto {

    @NotNull(message = "The email should not be null!")
    private String email;


    public MemberDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
