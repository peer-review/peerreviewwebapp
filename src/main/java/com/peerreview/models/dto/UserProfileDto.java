package com.peerreview.models.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserProfileDto{

    @NotNull(message = "The username should not be null!")
    @Size(min = 2, max = 20)
    private String username;

    @NotNull(message = "The email should not be null!")
    private String email;

    @NotNull(message = "Phone number should not be null!")
    @Pattern(regexp = "^\\d{10}$")
    private String phoneNumber;

    @Size(min = 8)
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
            , message = "Old password must be at least 8 symbols and should " +
            "contain capital letter, digit, and special symbol (+, -, *, &, ^, …)")
    private String currentPassword;

    @Size(min = 8)
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
            message = "Password must be at least 8 symbols and should " +
                    "contain capital letter, digit, and special symbol (+, -, *, &, ^, …)")
    private String password;

    @Size(min = 8)
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
            message = "Confirm password must be at least 8 symbols and should " +
                    "contain capital letter, digit, and special symbol (+, -, *, &, ^, …)")
    private String confirmPassword;

    public UserProfileDto() {
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}


