package com.peerreview.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserLoginDto {

    @NotNull(message = "The username should not be null!")
    @Size(min = 2, max = 20)
    private String username;


    @Size(min = 8)
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
            , message = "Password must be at least 8 symbols and should " +
            "contain capital letter, digit, and special symbol (+, -, *, &, ^, …)")
    private String password;

    public UserLoginDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
