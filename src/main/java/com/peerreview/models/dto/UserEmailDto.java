package com.peerreview.models.dto;

public class UserEmailDto {

    private String email;

    public UserEmailDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
