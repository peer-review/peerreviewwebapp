package com.peerreview.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TeamResultDto {
    @NotNull(message = "Team name should not be null!")
    @Size(min = 3, max = 30)
    private String name;
    private int ownerId;

    public TeamResultDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }
}
