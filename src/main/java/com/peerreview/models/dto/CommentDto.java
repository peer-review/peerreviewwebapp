package com.peerreview.models.dto;

import javax.validation.constraints.NotBlank;

public class CommentDto {
    @NotBlank(message = "Comment can't be blank")
    private String comment;

    public CommentDto() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
