package com.peerreview.models.dto;

import javax.validation.constraints.Positive;

public class StatusDto {
    @Positive(message = "Id can't be negative")
    private int id;

    public StatusDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
