package com.peerreview.models.dto;

public class SearchItemDto {

    private String title;

    private String status;

    private String creator;

    private String reviewer;

    private String team;

    private String sortParams;

    public SearchItemDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getSortParams() {
        return sortParams;
    }

    public void setSortParams(String sortParams) {
        this.sortParams = sortParams;
    }
}
