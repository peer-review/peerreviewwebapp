package com.peerreview.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ReviewerDto {
    @NotNull(message = "The username should not be null!")
    @Size(min = 2, max = 20)
    private String username;

    public ReviewerDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
