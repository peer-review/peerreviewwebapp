package com.peerreview.models.dto;

import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull(message = "The username should not be null!")
    @Size(min = 2, max = 20)
    private String username;

    @Size(min = 8, message = "The password size should be minimum 8 symbols")
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
            , message = "Password must be at least 8 symbols and should " +
            "contain capital letter, digit, and special symbol (+, -, *, &, ^, …)")
    private String password;

    @Size(min = 8)
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
            , message = "Confirm password must be at least 8 symbols and should " +
            "contain capital letter, digit, and special symbol (+, -, *, &, ^, …)")
    private String confirmPassword;


    @NotNull(message = "The email should not be null!")
    private String email;

    @NotNull(message = "Phone number should not be null!")
    @Pattern(regexp = "^\\d{10}$", message = "Phone number must be 10 digits!")
    private String phoneNumber;


    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


}
