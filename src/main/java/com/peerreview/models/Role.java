package com.peerreview.models;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "roles")
public class Role implements Serializable {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "role_id")
        private int id;

        @Column(name = "name")
        private String name;

        public Role() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }