package com.peerreview.models;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "versions")
public class Version {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "version_id")
    private int id;

    @Column(name = "item_id")
    private int itemId;

    @Column(name = "title")
    private String itemTitle;

    @Column(name = "description")
    private String itemDescription;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "creator_id")
    private User creator;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status")
    private Status status;

    @Column(name = "upload")
    private LocalDateTime upload;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

    public Version() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getUpload() {
        return upload;
    }

    public void setUpload(LocalDateTime upload) {
        this.upload = upload;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Version version = (Version) o;
        return id == version.id && itemId == version.itemId && Objects.equals(itemTitle, version.itemTitle) && Objects.equals(itemDescription, version.itemDescription) && Objects.equals(creator, version.creator) && Objects.equals(status, version.status) && Objects.equals(upload, version.upload) && Objects.equals(lastUpdate, version.lastUpdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, itemId, itemTitle, itemDescription, creator, status, upload, lastUpdate);
    }
}
