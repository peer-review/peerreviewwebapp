package com.peerreview.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "teams")
public class Team implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "team_id")
    private int id;

    @Column(name = "name")
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "owner_id")
    private User owner;

    @Column(name = "isDeleted")
    private boolean isDeleted;

    @Column(name = "minimum_reviewers")
    private int minimumReviewers;
    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "team_members",
            joinColumns = @JoinColumn(name = "team_id"),
            inverseJoinColumns = @JoinColumn(name = "member_id"))
    private Set<User> members = new HashSet<>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "team_work_items", joinColumns = @JoinColumn(name = "team_id"),inverseJoinColumns = @JoinColumn(name = "item_id"))
    private Set<WorkItem> workItem;


    public Team() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public int getMinimumReviewers() {
        return minimumReviewers;
    }

    public void setMinimumReviewers(int minimumReviewers) {
        this.minimumReviewers = minimumReviewers;
    }

    public Set<WorkItem> getWorkItem() {
        return workItem;
    }

    public void setWorkItem(Set<WorkItem> workItem) {
        this.workItem = workItem;
    }
}
