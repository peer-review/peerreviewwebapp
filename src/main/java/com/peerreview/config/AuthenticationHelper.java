package com.peerreview.config;

import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.models.User;
import com.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String REQUIRES_AUTHENTICATION = "The requested resource requires authentication.";
    public static final String INVALID_USERNAME = "Invalid username.";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";
    public static final String NO_USER_LOGGED_IN = "No user logged in.";
    public static final String CURRENT_USER = "currentUser";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, REQUIRES_AUTHENTICATION);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getUserByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_USERNAME);
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute(CURRENT_USER);

        if (currentUser == null) {
            throw new AuthenticationFailureException(NO_USER_LOGGED_IN);
        }

        return userService.getAnonymousUserByUsername(currentUser);
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getAnonymousUserByUsername(username);

            if (!user.getPassword().equals(String.valueOf(password.hashCode()))) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }
}
