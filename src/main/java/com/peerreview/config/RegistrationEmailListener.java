package com.peerreview.config;

import com.peerreview.models.OnRegistrationCompleteEvent;
import com.peerreview.models.User;
import com.peerreview.services.contracts.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import java.util.UUID;

@Component
public class RegistrationEmailListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private final VerificationTokenService verificationTokenService;
    private final MessageSource messages;
    private final MailSender mailSender;

    @Autowired
    public RegistrationEmailListener(VerificationTokenService verificationTokenService, MessageSource messages, MailSender mailSender) {
        this.verificationTokenService = verificationTokenService;
        this.messages = messages;
        this.mailSender = mailSender;
    }


    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        verificationTokenService.createVerificationToken(user, token);

        String recipient = user.getEmail();
        String subject = "Registration Confirmation";
        String url = event.getAppUrl() + "/auth/confirmRegistration/" + token.replace("-","");

        String message = "Thank you for registering. Please click on the below link to activate your account.";

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipient);
        email.setSubject(subject);
        email.setText(message + "http://localhost:8080" + url);
        System.out.println(url);
        mailSender.send(email);
    }
}
