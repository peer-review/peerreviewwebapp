package com.peerreview.repositories;

import com.peerreview.models.Role;
import com.peerreview.models.User;
import com.peerreview.repositories.contracts.RoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl extends AbstractCRUDRepository<Role> implements RoleRepository {


    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(Role.class, sessionFactory);

    }

    @Override
    public void create(Role entity) {

    }

    @Override
    public void update(Role entity) {
        super.update(entity);

    }
}
