package com.peerreview.repositories;

import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.models.Team;
import com.peerreview.models.User;
import com.peerreview.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getById(int userId) {

        if (super.getById(userId) == null) {
            throw new EntityNotFoundException("User", userId);
        }

        return super.getById(userId);
    }

    @Override
    public User getUserByUsername(String username) {
        return super.getByField("username",username);
    }

    @Override
    public List<User> search(String searchType) {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery("from User " +
                    "where username like :username or email like :email or phoneNumber like :phoneNumber", User.class);

            query.setParameter("username", "%" + searchType + "%");
            query.setParameter("email", "%" + searchType + "%");
            query.setParameter("phoneNumber", "%" + searchType + "%");

            return query.getResultList();
        }
    }

    @Override
    public User getUserPhoto(String photo) {

      return super.getByField("photo",photo);
    }

    @Override
    public User getUserByEmail(String email) {
        return super.getByField("email",email);

    }

    @Override
    public void update(User user) {
        super.update(user);
    }


    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery("from User where isDeleted = false ", User.class);


            return query.list();
        }
    }
}
