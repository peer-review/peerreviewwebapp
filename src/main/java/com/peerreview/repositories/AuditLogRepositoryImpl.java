package com.peerreview.repositories;

import com.peerreview.repositories.contracts.AuditLogRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

@Repository
public class AuditLogRepositoryImpl implements AuditLogRepository {

    @Value("${database.url}")
    private String dbUrl;
    @Value("${database.username}")
    private String dbUsername;
    @Value("${database.password}")
    private String dbPassword;
    private final SessionFactory sessionFactory;

    @Autowired
    public AuditLogRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<String> getAll() {
        String query = "select log " +
                "from audit_log order by log_id desc";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query)
        ) {
            return getLogs(resultSet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void create(String log) {
        String query = "insert into audit_log (log) " +
                "values (?)";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, log);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private List<String> getLogs(ResultSet resultSet) throws SQLException {
        List<String> logs = new LinkedList<>();
        while (resultSet.next()) {
            String log = resultSet.getString("log");
            logs.add(log);
        }
        return logs;
    }
}
