package com.peerreview.repositories;

import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.models.Reviewer;
import com.peerreview.repositories.contracts.ReviewerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class ReviewerRepositoryImpl extends AbstractCRUDRepository<Reviewer> implements ReviewerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ReviewerRepositoryImpl(SessionFactory sessionFactory) {
        super(Reviewer.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Reviewer getByItemAndUsername(int itemId, String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<Reviewer> query = session.createQuery(
                    "from Reviewer where workItem.id = :itemId and reviewer.username =: username"
            );
            query.setParameter("itemId", itemId);
            query.setParameter("username", username);

            return query.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }
}
