package com.peerreview.repositories;

import com.peerreview.models.Status;
import com.peerreview.repositories.contracts.StatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class StatusRepositoryImpl extends AbstractReadRepository<Status> implements StatusRepository {
    private final SessionFactory sessionFactory;

    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        super(Status.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Status getByName(String name) {
        return super.getByField("name", name);
    }
}
