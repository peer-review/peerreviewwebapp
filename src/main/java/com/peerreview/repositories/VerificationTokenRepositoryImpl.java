package com.peerreview.repositories;

import com.peerreview.models.VerificationToken;

import com.peerreview.repositories.contracts.VerificationTokenRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class VerificationTokenRepositoryImpl extends AbstractCRUDRepository<VerificationToken> implements VerificationTokenRepository {

    public VerificationTokenRepositoryImpl(SessionFactory sessionFactory) {
        super(VerificationToken.class, sessionFactory);
    }

    @Override
    public VerificationToken findByToken(String verificationToken) {
        return super.getByField("token",verificationToken);
    }
}
