package com.peerreview.repositories;

import com.peerreview.models.Version;
import com.peerreview.repositories.contracts.VersionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VersionRepositoryImpl extends AbstractCRUDRepository<Version> implements VersionRepository {

    private final SessionFactory sessionFactory;

    public VersionRepositoryImpl(SessionFactory sessionFactory) {
        super(Version.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Version getByTitle(String title) {
        return super.getByField("title", title);
    }

    @Override
    public List<Version> getAllVersionOfItem(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Version> query = session.createQuery(
                    "from Version where itemId = :id order by id desc"
            );
            query.setParameter("id", id);

            return query.list();
        }
    }
}
