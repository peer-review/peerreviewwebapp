package com.peerreview.repositories;

import com.peerreview.models.Comment;
import com.peerreview.repositories.contracts.CommentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentRepositoryImpl extends AbstractCRUDRepository<Comment> implements CommentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CommentRepositoryImpl(SessionFactory sessionFactory) {
        super(Comment.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Comment> itemComments(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery(
                    "select distinct c from Comment c join c.workItem w where w.id = :itemId order by c.id desc"
            );
            query.setParameter("itemId", id);

            return query.list();
        }
    }
}
