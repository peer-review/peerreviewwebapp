package com.peerreview.repositories;

import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.models.Team;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.repositories.contracts.TeamRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

@Repository
public class TeamRepositoryImpl extends AbstractCRUDRepository<Team> implements TeamRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public TeamRepositoryImpl(SessionFactory sessionFactory) {
        super(Team.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void delete(int id) {

    }

    @Override
    public Team getById(int id) {
        return super.getByField("id", id);
    }

    @Override
    public Team getByTeamName(String name) {

        return super.getByField("name", name);
    }

    @Override
    public User getByOwnerId(int memberId) {

        try (Session session = sessionFactory.openSession()) {

            Query<Team> query = session.createQuery(" from Team.owner where id =:memberId ", Team.class);

            User owner = query.getSingleResult().getOwner();

            if (owner == null) {
                throw new EntityNotFoundException("Owner", "id", String.valueOf(memberId));
            }
            return owner;

        }

    }

    @Override
    public void addMember(Team team, User member) {

        try (Session session = sessionFactory.openSession()) {

            team.getMembers().add(member);
            super.update(team);
        }

    }

    @Override
    public User getTeamMemberById(int memberId) {
        try (Session session = sessionFactory.openSession()) {

            NativeQuery<User> query = session.createNativeQuery("select * from team_members tm " +
                    "join users u on tm.member_id = u.user_id " +
                    "where u.user_id = :memberId");

            query.setParameter("memberId", memberId);
            query.addEntity(User.class);

            return query.list().get(0);
        }
    }


    @Override
    public Team getAllPendingWorkItems(Team team) {

        try (Session session = sessionFactory.openSession()) {

            NativeQuery<Team> query = session.createNativeQuery("select *\n" +
                    "from teams\n" +
                    "join team_work_items twi on teams.team_id = twi.team_id\n" +
                    "join work_items_statuses wis on twi.item_id = wis.item_id\n" +
                    "join statuses s on s.status_id = wis.status_id\n" +
                    "where s.name like 'Pending'");

            query.addEntity(WorkItem.class);
            return query.getSingleResult();
        }
    }

    @Override
    public List<User> getAllTeamMembers(Team team) {
        try (Session session = sessionFactory.openSession()) {


            NativeQuery<User> query = session.createNativeQuery("select *\n" +
                    "from team_members as tm\n" +
                    "join teams t on tm.team_id = t.team_id\n" +
                    "join users u on tm.member_id = u.user_id\n" +
                    "where t.team_id = :team.getId");

            query.setParameter("team.getId", team.getId());
            query.addEntity(User.class);

            return query.list();

        }
    }

    @Override
    public List<Team> getAllOfTheGivenOwner(User user) {

        try (Session session = sessionFactory.openSession()) {

            Query<Team> query = session.createQuery("from Team where owner.username like :username and isDeleted = false ", Team.class);

            query.setParameter("username", user.getUsername());

            return query.list();
        }

    }

    @Override
    public List<Team> getTeamsOfUser(User user) {
        List<Team> teams = new LinkedList<>();
        try (Session session = sessionFactory.openSession()) {
            Query<Team> owner = session.createQuery("select distinct t from Team t join t.members m where m.username like :username and t.isDeleted = false", Team.class);
            Query<Team> member = session.createQuery("select distinct t from Team t where t.owner.username like :username and t.isDeleted = false", Team.class);

            owner.setParameter("username", user.getUsername());
            member.setParameter("username", user.getUsername());

            teams.addAll(owner.list());
            teams.addAll(member.list());
            return teams;
        }
    }
}