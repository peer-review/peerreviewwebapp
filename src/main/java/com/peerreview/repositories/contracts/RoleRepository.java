package com.peerreview.repositories.contracts;

import com.peerreview.models.Role;
import com.peerreview.models.User;

public interface RoleRepository extends BaseCrudRepository<Role> {
}
