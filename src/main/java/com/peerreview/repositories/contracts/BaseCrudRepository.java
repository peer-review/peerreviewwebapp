package com.peerreview.repositories.contracts;

import com.peerreview.models.WorkItem;

public interface BaseCrudRepository<T> extends BaseReadRepository<T>{
    void create(T entity);

    void update(T entity);

    void delete(int id);
}
