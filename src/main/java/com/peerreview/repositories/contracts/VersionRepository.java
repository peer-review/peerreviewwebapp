package com.peerreview.repositories.contracts;

import com.peerreview.models.Version;

import java.util.List;

public interface VersionRepository extends BaseCrudRepository<Version> {
    Version getByTitle(String title);

    List<Version> getAllVersionOfItem(int id);
}
