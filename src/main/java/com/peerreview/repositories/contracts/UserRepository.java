package com.peerreview.repositories.contracts;

import com.peerreview.models.User;

import java.util.List;

public interface UserRepository extends BaseCrudRepository<User> {
    User getUserByUsername(String username);

    List<User> search(String searchType);

    User getUserPhoto(String photo);

    User getUserByEmail(String email);
}
