package com.peerreview.repositories.contracts;

import com.peerreview.models.VerificationToken;

public interface VerificationTokenRepository extends BaseCrudRepository<VerificationToken> {

    VerificationToken findByToken(String verificationToken);

    void create(VerificationToken myToken);

}
