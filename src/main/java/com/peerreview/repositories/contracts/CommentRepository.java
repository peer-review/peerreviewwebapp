package com.peerreview.repositories.contracts;

import com.peerreview.models.Comment;

import java.util.List;

public interface CommentRepository extends BaseCrudRepository<Comment>{

    List<Comment> itemComments(int id);
}
