package com.peerreview.repositories.contracts;

import java.util.List;

public interface AuditLogRepository {
    List<String> getAll();

    void create(String log);
}
