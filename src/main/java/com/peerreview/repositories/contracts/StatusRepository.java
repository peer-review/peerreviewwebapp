package com.peerreview.repositories.contracts;

import com.peerreview.models.Status;

public interface StatusRepository extends BaseReadRepository<Status>  {
    Status getByName(String name);
}
