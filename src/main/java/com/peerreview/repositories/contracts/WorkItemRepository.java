package com.peerreview.repositories.contracts;

import com.peerreview.models.Status;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;

import java.util.List;
import java.util.Optional;

public interface WorkItemRepository extends BaseCrudRepository<WorkItem> {

    WorkItem getByTitle(String title);

    List<WorkItem> search(Optional<String> search);

    List<WorkItem> filter(Optional<String> title, Optional<String> status, Optional<String> team, Optional<String> creator, Optional<String> reviewer, Optional<String> sort);

    List<Status> getAllStatusesOfItem(int id);

    List<WorkItem> getDrafts(User user);
}
