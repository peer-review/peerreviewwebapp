package com.peerreview.repositories.contracts;

import com.peerreview.models.Reviewer;

public interface ReviewerRepository extends BaseCrudRepository<Reviewer> {
    Reviewer getByItemAndUsername(int itemId, String username);
}
