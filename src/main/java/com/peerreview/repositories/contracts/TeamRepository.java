package com.peerreview.repositories.contracts;

import com.peerreview.models.Team;
import com.peerreview.models.User;

import java.util.List;

public interface TeamRepository extends BaseCrudRepository<Team>{
    Team getByTeamName(String name);

    User getByOwnerId(int memberId);


    void  addMember(Team team, User member);


    User getTeamMemberById(int memberId);


    Team getAllPendingWorkItems(Team team);


    List<User> getAllTeamMembers(Team team);

    List<Team> getAllOfTheGivenOwner(User user);

    List<Team> getTeamsOfUser(User user);
}
