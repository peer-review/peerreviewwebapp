package com.peerreview.repositories;

import com.peerreview.models.Status;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.repositories.contracts.WorkItemRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class WorkItemRepositoryImpl extends AbstractCRUDRepository<WorkItem> implements WorkItemRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public WorkItemRepositoryImpl(SessionFactory sessionFactory) {
        super(WorkItem.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public WorkItem getByTitle(String title) {
        return super.getByField("title", title);
    }

    @Override
    public WorkItem getById(int id) {
        return super.getByField("deleted = false and id", id);
    }

    @Override
    public List<WorkItem> search(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            Query<WorkItem> query = session.createQuery(
                    "select distinct w from WorkItem w join w.reviewers r where w.id = :id or w.creator.id =: creatorId or w.team.id = :teamId or w.title like :title or r.id =: reviewerId and w.deleted = false"
            );
            query.setParameter("id", checkIfStringIsInt(search.get()));
            query.setParameter("creatorId", checkIfStringIsInt(search.get()));
            query.setParameter("reviewerId", checkIfStringIsInt(search.get()));
            query.setParameter("teamId", checkIfStringIsInt(search.get()));
            query.setParameter("title", '%' + search.get() + '%');

            return query.list();
        }
    }

    @Override
    public List<WorkItem> filter(Optional<String> title, Optional<String> status, Optional<String> team, Optional<String> creator, Optional<String> reviewer, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder();
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            filters.add("w.team != null and deleted = false");

            if (title.isPresent() && !title.get().equals("")) {
                filters.add("w.title like :title");
                params.put("title", '%' + title.get() + '%');
            }

            if (status.isPresent() && !status.get().equals("")) {
                filters.add("w.status.name like :status");
                params.put("status", status.get());
            }

            if (team.isPresent() && !team.get().equals("")) {
                filters.add("w.team.name like :team");
                params.put("team", team.get());
            }

            if (creator.isPresent() && !creator.get().equals("")) {
                filters.add("w.creator.username like :creator");
                params.put("creator", '%' + creator.get() + '%');
            }

            if (reviewer.isPresent() && !reviewer.get().equals("")) {
                filters.add("r.username like :reviewer");
                params.put("reviewer", '%' + reviewer.get() + '%');
            }

            queryString.append("select distinct w from WorkItem w join w.reviewers r ");
            queryString.append(" where ").append(String.join(" and ", filters));

            sort.ifPresent(value -> {
                queryString.append(generateSortingString(value));
            });
            Query<WorkItem> query = session.createQuery(queryString.toString(), WorkItem.class);
            query.setProperties(params);
            return query.list();
        }
    }

    @Override
    public List<Status> getAllStatusesOfItem(int id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Status> query = session.createNativeQuery(
                    "select s.status_id, name" +
                            "             from reviewers_items w" +
                            "                 join statuses s on s.status_id = w.status_id" +
                            "             where item_id = :item.id"
                    , Status.class);
            query.setParameter("item.id", id);
            return query.list();
        }
    }

    @Override
    public List<WorkItem> getDrafts(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<WorkItem> query = session.createQuery(
                    "from WorkItem where team = null and creator.username =: username and deleted = false order by id desc"
            );
            query.setParameter("username", user.getUsername());

            return query.list();
        }
    }

    private String generateSortingString(String value) {
        StringBuilder result = new StringBuilder(" order by ");
        var params = value.toLowerCase().split("_");

        switch (params[0]) {
            case "recently":
                result.append("w.id desc");
                break;
            case "oldest":
                result.append("w.id");
                break;
            case "name ↑":
                result.append("w.title ");
                break;
            case "name ↓":
                result.append("w.title desc");
                break;
            case "status ↑":
                result.append("w.status.name ");
                break;
            case "status ↓":
                result.append("w.status.name desc");
                break;
            default:
                return "";
        }

        return result.toString();
    }

    public int checkIfStringIsInt(String search) {
        try {
            return Integer.parseInt(search);
        } catch (NumberFormatException e) {
            return -1;
        }
    }
}
