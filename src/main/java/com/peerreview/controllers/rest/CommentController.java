package com.peerreview.controllers.rest;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.CommentModelMapper;
import com.peerreview.models.Comment;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.CommentDto;
import com.peerreview.services.contracts.CommentService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;
    private final WorkItemService workItemService;
    private final CommentModelMapper commentModelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CommentController(CommentService commentService, WorkItemService workItemService, CommentModelMapper commentModelMapper, AuthenticationHelper authenticationHelper) {
        this.commentService = commentService;
        this.workItemService = workItemService;
        this.commentModelMapper = commentModelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{itemId}")
    public List<Comment> getItemComments(@PathVariable int itemId, @RequestHeader HttpHeaders headers) {
        WorkItem workItem = workItemService.getById(itemId, authenticationHelper.tryGetUser(headers));
        return new ArrayList<>(workItem.getComments());
    }

    @PostMapping("/{itemId}")
    public Comment postComment(@PathVariable int itemId, @Valid @RequestBody CommentDto commentDto, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        WorkItem workItem = workItemService.getById(itemId, user);
        Comment comment = commentModelMapper.fromDto(commentDto, user, workItem);
        commentService.addCommentToTask(workItem, comment);
        return comment;
    }

}
