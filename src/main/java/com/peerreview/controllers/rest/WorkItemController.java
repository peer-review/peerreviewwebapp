package com.peerreview.controllers.rest;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.ReviewerModelMapper;
import com.peerreview.controllers.mappers.WorkItemModelMapper;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Reviewer;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.WorkItemDtoBase;
import com.peerreview.models.dto.WorkItemDtoMvc;
import com.peerreview.services.contracts.ReviewerService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/items")
public class WorkItemController {

    private final WorkItemService workItemService;
    private final WorkItemModelMapper workItemModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ReviewerService reviewerService;
    private final ReviewerModelMapper reviewerModelMapper;

    @Autowired
    public WorkItemController(WorkItemService workItemService, WorkItemModelMapper workItemModelMapper, AuthenticationHelper authenticationHelper, ReviewerService reviewerService, ReviewerModelMapper reviewerModelMapper) {
        this.workItemService = workItemService;
        this.workItemModelMapper = workItemModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.reviewerService = reviewerService;
        this.reviewerModelMapper = reviewerModelMapper;
    }

    @GetMapping
    public List<WorkItem> getAll(@RequestParam(required = false) Optional<String> search, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return workItemService.search(search, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @GetMapping("/{id}")
    public WorkItem getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return workItemService.getById(id, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public WorkItem createWorkItem(@Valid @RequestBody WorkItemDtoMvc workItemDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            WorkItem workItem = workItemModelMapper.fromDtoMvc(workItemDto, user);
            workItemService.create(workItem, user);
            return workItem;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public WorkItem updateWorkItem(@PathVariable int id, @Valid @RequestBody WorkItemDtoBase workItemDto,
                                   @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            WorkItem workItem = workItemModelMapper.fromDto(workItemDto, id, user);
            workItemService.update(workItem, user);
            return workItem;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{itemId}/status/{statusName}")
    public WorkItem changeStatus(@PathVariable int itemId, @PathVariable String statusName, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            WorkItem workItem = workItemService.getById(itemId, user);
            Reviewer reviewer = reviewerModelMapper.fromDtoWithStatus(user.getUsername(), workItem, statusName);
            reviewerService.update(reviewer, user);
            return workItem;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            workItemService.delete(id, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<WorkItem> filter(@RequestParam(required = false) Optional<String> title,
                                 @RequestParam(required = false) Optional<String> status,
                                 @RequestParam(required = false) Optional<String> team,
                                 @RequestParam(required = false) Optional<String> creator,
                                 @RequestParam(required = false) Optional<String> reviewer,
                                 @RequestParam(required = false) Optional<String> sort,
                                 @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return workItemService.filter(title, status, team, creator, reviewer, sort, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
