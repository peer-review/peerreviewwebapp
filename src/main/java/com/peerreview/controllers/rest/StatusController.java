package com.peerreview.controllers.rest;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.models.Status;
import com.peerreview.models.User;
import com.peerreview.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/statuses")
public class StatusController {

    private final StatusService statusService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public StatusController(StatusService statusService, AuthenticationHelper authenticationHelper) {
        this.statusService = statusService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Status> getAll(@RequestHeader HttpHeaders headers) {
        return statusService.getAll(authenticationHelper.tryGetUser(headers));
    }

    @GetMapping("/{id}")
    public Status getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        return statusService.getById(id, authenticationHelper.tryGetUser(headers));
    }
}
