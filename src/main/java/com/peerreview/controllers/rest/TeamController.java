package com.peerreview.controllers.rest;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.TeamModelMapper;
import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Team;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.TeamDto;
import com.peerreview.services.contracts.TeamService;
import com.peerreview.services.contracts.UserService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/teams")
public class TeamController {

    private final TeamService teamService;
    private final AuthenticationHelper authenticationHelper;
    private final TeamModelMapper teamModelMapper;
    private final UserService userService;
    private final WorkItemService workItemService;

    @Autowired
    public TeamController(TeamService teamService, AuthenticationHelper authenticationHelper, TeamModelMapper teamModelMapper, UserService userService, WorkItemService workItemService) {
        this.teamService = teamService;
        this.authenticationHelper = authenticationHelper;
        this.teamModelMapper = teamModelMapper;
        this.userService = userService;
        this.workItemService = workItemService;
    }

    @GetMapping()
    public List<Team> getAll(@RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            return teamService.getAll(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Team getById(@Valid @PathVariable int id, @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            return teamService.getById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping()
    public Team create(@Valid @RequestBody TeamDto teamDto, @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            Team team = teamModelMapper.fromDto(teamDto);

            teamService.create(team, user);
            return team;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{teamId}/member={memberId}")
    public Team addMember(@PathVariable int teamId, @PathVariable int memberId, @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            return teamService.addMember(teamId, memberId, user);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @PutMapping("/remove/{teamId}/member={memberId}")
    public Team removeMember(@PathVariable int teamId, @PathVariable int memberId, @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            return teamService.removeMember(teamId, memberId, user);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @GetMapping("/teamMembers={teamId}")
    public List<User> getAllTeamMembers(@Valid @PathVariable int teamId, @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            return teamService.getAllTeamMembers(teamId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @PutMapping("{id}")
    public Team update(@Valid @PathVariable int id, @RequestBody TeamDto teamDto, @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Team team = teamModelMapper.fromDto(teamDto, id);

            teamService.update(team, user);
            return team;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{teamId}/addWorkItem={itemId}")
    public Team addWorkingItem(@Valid @PathVariable int teamId, @Valid @PathVariable int itemId, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Team team = teamService.getById(teamId, user);
            WorkItem workItem = workItemService.getById(itemId, user);

            teamService.addWorkItemToTeam(team, workItem, user);

            return team;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @GetMapping("pendingWorkItems{teamId}")
    public Team getAllPendingWorkItems(@Valid @PathVariable int teamId, @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Team team = teamService.getById(teamId, user);


            return  teamService.getAllPendingWorkItems(team,user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }
    @DeleteMapping("{teamId}")
    public void deleteTeam(@PathVariable("teamId") int teamId, HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Team teamToDelete = teamService.getById(teamId,user);
            teamService.delete(teamToDelete.getId(),user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}