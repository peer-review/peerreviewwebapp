package com.peerreview.controllers.rest;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.UserModelMapper;
import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.User;
import com.peerreview.models.dto.UserDto;
import com.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;

    @Autowired
    public UserController(UserService userService, AuthenticationHelper authenticationHelper, UserModelMapper userModelMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
    }


    @GetMapping()
    public List<User> getAll(@RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getAll(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User getById(@Valid @PathVariable int id, @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping()
    public User create(@Valid @RequestBody UserDto userDto) {

        try {
            User user = userModelMapper.fromDto(userDto);
            userService.register(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public User update(@Valid @PathVariable int id, @Valid @RequestBody UserDto userDto, @RequestHeader HttpHeaders headers) {


        try {
            User user = authenticationHelper.tryGetUser(headers);
            User updateUser = userModelMapper.fromDto(userDto, id);
            userService.update(updateUser, user);
            return updateUser;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/changeRole={userId}")
    public User changeRole (@Valid @PathVariable int userId, @RequestHeader HttpHeaders headers){

        try {
            User user = authenticationHelper.tryGetUser(headers);
            User userToChange = userService.getById(userId,user);
            userService.create(userToChange,user);
            return userToChange;
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }catch (UnauthorizedOperationException e){
            throw  new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }
    @GetMapping("/search")
    public List<User> search (@RequestParam(required = false) Optional<String> searchType, @RequestHeader HttpHeaders headers){

        try {
            User user = authenticationHelper.tryGetUser(headers);

           return userService.search(searchType,user);
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }

    }
    @DeleteMapping("/{id}")
    public void delete (@Valid @PathVariable int id, @RequestHeader HttpHeaders headers){

        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.delete(id, user);
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }

    }

}