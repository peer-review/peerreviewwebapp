package com.peerreview.controllers.mvc;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.ReviewerModelMapper;
import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Reviewer;
import com.peerreview.models.Status;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.ReviewerDto;
import com.peerreview.models.dto.StatusDto;
import com.peerreview.services.contracts.ReviewerService;
import com.peerreview.services.contracts.StatusService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("/peerreview/{itemId}/reviewer")
public class ReviewerMvcController {

    private final WorkItemService workItemService;
    private final AuthenticationHelper authenticationHelper;
    private final ReviewerService reviewerService;
    private final ReviewerModelMapper reviewerModelMapper;
    private final StatusService statusService;

    public ReviewerMvcController(WorkItemService workItemService, AuthenticationHelper authenticationHelper, ReviewerService reviewerService, ReviewerModelMapper reviewerModelMapper, StatusService statusService) {
        this.workItemService = workItemService;
        this.authenticationHelper = authenticationHelper;
        this.reviewerService = reviewerService;
        this.reviewerModelMapper = reviewerModelMapper;
        this.statusService = statusService;
    }

    @GetMapping()
    public String showAddReviewerPage(Model model, @PathVariable("itemId") int itemId, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            WorkItem workItem = workItemService.getById(itemId, user);

            model.addAttribute("reviewerDto", new ReviewerDto());
            model.addAttribute("reviewers", getItemReviewers(workItem));
            return "item-reviewer";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

    }

    @PostMapping()
    public String addReviewer(@PathVariable("itemId") int itemId, @Valid @ModelAttribute ReviewerDto reviewerDto, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            WorkItem workItem = workItemService.getById(itemId, user);
            if (reviewerDto.getUsername().equals("1")) {
                return "redirect:/peerreview/" + workItem.getTeam().getId() + "/items/" + workItem.getId();
            }
            Reviewer reviewer = reviewerModelMapper.fromDto(reviewerDto, workItem);
            reviewerService.create(reviewer, user);
            return "redirect:/peerreview/" + workItem.getTeam().getId() + "/items/" + workItem.getId();
        } catch (EntityNotFoundException | DuplicateEntityException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/status")
    public String updateStatus(@PathVariable("itemId") int itemId, @Valid @ModelAttribute StatusDto statusDto, BindingResult errors, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            WorkItem workItem = workItemService.getById(itemId, user);
            if (errors.hasErrors()) {
                return "redirect:/peerreview/" + workItem.getTeam().getId() + "/items/" + workItem.getId();
            }
            Status statusToUpdate = statusService.getById(statusDto.getId(), user);
            Reviewer reviewer = reviewerModelMapper.fromDtoWithStatus(user.getUsername(), workItem, statusToUpdate);
            reviewerService.update(reviewer, user);
            if (statusToUpdate.getName().equalsIgnoreCase("rejected")) {
                return "redirect:/peerreview/" + workItem.getId() + "/comments";
            }
            return "redirect:/peerreview/" + workItem.getTeam().getId() + "/items/" + workItem.getId();
        } catch (EntityNotFoundException | DuplicateEntityException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/username={name}")
    public String removeReviewer(@PathVariable("itemId") int itemId, @PathVariable("name") String name, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            WorkItem workItem = workItemService.getById(itemId, user);
            Reviewer reviewerToRemove = reviewerService.getByItemAndUsername(workItem.getId(), name);
            reviewerService.delete(reviewerToRemove.getId(), user);
            workItemService.updateStatus(workItem);

            return "redirect:/peerreview/" + workItem.getTeam().getId() + "/items/" + workItem.getId();
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("user")
    public User populateUser(HttpSession session) {
        return authenticationHelper.tryGetUser(session);
    }

    private Set<User> getItemReviewers(WorkItem workItem) {
        Set<User> itemReviewers = new HashSet<>(workItem.getTeam().getMembers());
        itemReviewers.add(workItem.getTeam().getOwner());
        itemReviewers.remove(workItem.getCreator());
        return itemReviewers;
    }
}
