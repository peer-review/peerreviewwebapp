package com.peerreview.controllers.mvc;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.WorkItemModelMapper;
import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.*;
import com.peerreview.models.dto.*;
import com.peerreview.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Controller
@RequestMapping("/peerreview/{teamId}/items")
public class WorkItemMvcController {

    private final WorkItemService workItemService;
    private final UserService userService;
    private final WorkItemModelMapper workItemModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final TeamService teamService;
    private final StatusService statusService;
    private final ReviewerService reviewerService;
    private final CommentService commentService;
    private final VersionService versionService;
    @Value("${upload.folder}")
    String UPLOADED_FOLDER;

    @Autowired
    public WorkItemMvcController(WorkItemService workItemService, UserService userService, WorkItemModelMapper workItemModelMapper, AuthenticationHelper authenticationHelper, TeamService teamService, StatusService statusService, ReviewerService reviewerService, CommentService commentService, VersionService versionService) {
        this.workItemService = workItemService;
        this.userService = userService;
        this.workItemModelMapper = workItemModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.teamService = teamService;
        this.statusService = statusService;
        this.reviewerService = reviewerService;
        this.commentService = commentService;
        this.versionService = versionService;
    }

    @GetMapping()
    public String showTeamItems(HttpSession session, Model model, @PathVariable("teamId") int id) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Team team = teamService.getById(id, user);
            List<WorkItem> workItems = workItemService.filter(
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(team.getName()),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of("Recently"),
                    user
            );
            model.addAttribute("items", workItems);
            return "items-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping()
    public String showTeamItemsFiltered(@ModelAttribute SearchItemDto searchItemDto, @PathVariable("teamId") int teamId, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Team team = teamService.getById(teamId, user);
            List<WorkItem> workItems = workItemService.filter(
                    Optional.of(searchItemDto.getTitle()),
                    Optional.of(searchItemDto.getStatus()),
                    Optional.of(team.getName()),
                    Optional.of(searchItemDto.getCreator()),
                    Optional.of(searchItemDto.getReviewer()),
                    Optional.of(searchItemDto.getSortParams()),
                    user
            );
            model.addAttribute("items", workItems);
            return "items-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable int id, @PathVariable("teamId") int teamId, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            WorkItem workItem = workItemService.getById(id, user);
            Team team = teamService.getById(teamId, user);
            model.addAttribute("item", workItem);
            model.addAttribute("files", listFiles(id));
            model.addAttribute("status", workItemService.getFinalStatusOfAllReviewers(id));
            model.addAttribute("reviewersStatuses", getReviewers(workItem));
            model.addAttribute("reviewers", getPossibleReviewers(workItem.getCreator(), team));
            model.addAttribute("comments", commentService.itemComments(id));
            return "item-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/create")
    public String showNewItemPage(@PathVariable("teamId") int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Team team = teamService.getById(id, user);

            model.addAttribute("minimumReviewers", team.getMinimumReviewers());
            model.addAttribute("reviewers", getPossibleReviewers(user, team));
            model.addAttribute("workItemDto", new WorkItemDtoMvc());
            return "item-create";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/create")
    public String createNewItem(@Valid @ModelAttribute WorkItemDtoMvc workItemDto, BindingResult errors, @PathVariable("teamId") int teamId, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if (errors.hasErrors()) {
                errors.rejectValue("description", "400", "Length is too small.");
                return "redirect:/peerreview/" + teamId + "/items/create";
            }
            if (Arrays.asList(workItemDto.getReviewers()).contains("-1")) {
                errors.rejectValue("reviewers", "400", "Must choose reviewer.");
                return "redirect:/peerreview/" + teamId + "/items/create";
            }
            WorkItem workItem = workItemModelMapper.fromDtoMvc(workItemDto, user);
            workItemService.create(workItem, user);

            Team team = teamService.getById(teamId, user);
            teamService.addWorkItemToTeam(team, workItem, user);
            return "redirect:/peerreview/" + teamId + "/items/" + workItem.getId();
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("title", "duplicate_item", e.getMessage());
            return "item-create";
        }
    }

    @GetMapping("/update={itemId}")
    public String updateItemPage(HttpSession session, Model model, @PathVariable("teamId") int teamId, @PathVariable("itemId") int itemId) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            WorkItemDtoBase workItemDto = workItemModelMapper.toDto(workItemService.getById(itemId, user));
            model.addAttribute("dto", workItemDto);
            return "item-update";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

    }

    @PostMapping("/update={itemId}")
    public String updateItem(@Valid @ModelAttribute WorkItemDtoBase workItemDtoBase, BindingResult errors, @PathVariable("itemId") int itemId, @PathVariable("teamId") int teamId, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if (errors.hasErrors()) {
                errors.rejectValue("description", "400", "Length is too small.");
                return "redirect:/peerreview/" + teamId + "/items/update=" + itemId;
            }
            WorkItem workItemToUpdate = workItemModelMapper.fromDtoBase(workItemDtoBase, workItemService.getById(itemId, user));
            workItemService.update(workItemToUpdate, user);
            return "redirect:/peerreview/" + teamId + "/items/" + workItemToUpdate.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/delete={itemId}")
    public String deleteItem(HttpSession session, @PathVariable("teamId") int teamId, @PathVariable("itemId") int itemId) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            boolean isDraft = isDraft(workItemService.getById(itemId, user));
            workItemService.delete(itemId, user);
            if (isDraft) {
                return "redirect:/peerreview/items/drafts";
            }
            return "redirect:/peerreview/" + teamId + "/items";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/download/{param}")
    public void downloadFile(HttpSession session, Model model, HttpServletResponse response,
                             @PathVariable("param") String path) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return;
        } catch (AuthenticationFailureException e) {
            return;
        }

        String[] param = path.split("~");
        int id = Integer.parseInt(param[0]);
        String fileName = param[1];

        String dataDirectory = UPLOADED_FOLDER + id + "//";
        Path file = Paths.get(dataDirectory, fileName);
        if (Files.exists(file)) {
            String[] extension = fileName.split("\\.");
            response.setContentType("application/" + extension[extension.length - 1]);
            response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
            try {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @GetMapping("/delete/{param}")
    public String deleteFile(Model model, HttpSession session,
                             @PathVariable("param") String teamPlusFile,
                             @PathVariable("teamId") int teamId) {
        User user = null;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        String[] param = teamPlusFile.split("~");
        int id = Integer.parseInt(param[0]);
        String fileName = param[1];

        File fileToDelete = new File(UPLOADED_FOLDER + id + "\\" + fileName);
        fileToDelete.delete();

        if (isDraft(workItemService.getById(id, user))) {
            return "redirect:/peerreview/items/draft=" + id;
        }
        return "redirect:/peerreview/" + teamId + "/items/" + id;
    }

    @PostMapping("/upload/{param}")
    public String fileUpload(Model model, HttpSession session,
                             @RequestParam("file") MultipartFile file,
                             RedirectAttributes redirectAttributes,
                             @PathVariable("param") int id,
                             @PathVariable("teamId") int teamId) {
        User user = null;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        WorkItem workItem = workItemService.getById(id, user);
        if (file.isEmpty() && !isDraft(workItem)) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:/peerreview/" + teamId + "/items/" + id;
        } else if (file.isEmpty() && isDraft(workItem)) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:/peerreview/items/draft=" + id;
        }

        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + id + "//" + file.getOriginalFilename());
            Files.write(path, bytes);
        } catch (IOException ignored) {
        }

        if (isDraft(workItem)) {
            return "redirect:/peerreview/items/draft=" + id;
        }
        return "redirect:/peerreview/" + teamId + "/items/" + id;
    }

    @GetMapping("/versions={itemId}")
    public String showVersionsPage(@PathVariable("itemId") int id, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);

            model.addAttribute("items", versionService.getAllVersionOfItem(id));
            return "versions-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/version={id}")
    public String getByIdVersion(@PathVariable int id, @PathVariable("teamId") int teamId, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Version version = versionService.getById(id, user);

            model.addAttribute("item", version);
            return "version-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @ModelAttribute("commentDto")
    public CommentDto populateCommentDto() {
        return new CommentDto();
    }

    @ModelAttribute("reviewerDto")
    public ReviewerDto populateReviewerDto() {
        return new ReviewerDto();
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses(HttpSession session) {
        return statusService.getAll(authenticationHelper.tryGetUser(session));
    }

    @ModelAttribute("statusDto")
    public StatusDto populateStatusDto(HttpSession session) {
        return new StatusDto();
    }

    @ModelAttribute("searchItemDto")
    public SearchItemDto populateSearchItemDto() {
        return new SearchItemDto();
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("Recently", "Oldest", "Name ↑", "Name ↓", "Status ↑", "Status ↓"));
    }

    @ModelAttribute("user")
    public User populateUser(HttpSession session) {
        return authenticationHelper.tryGetUser(session);
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    private File[] listFiles(int id) {
        File dir = new File(UPLOADED_FOLDER + id + "//");
        return dir.listFiles();
    }

    private Set<User> getPossibleReviewers(User notPermitted, Team team) {
        Set<User> allPossibleReviewers = new HashSet<>(team.getMembers());
        allPossibleReviewers.add(team.getOwner());
        allPossibleReviewers.remove(notPermitted);
        return allPossibleReviewers;
    }

    private Set<Reviewer> getReviewers(WorkItem workItem) {
        Set<User> itemReviewers = new HashSet<>(workItem.getReviewers());

        Set<Reviewer> reviewers = new HashSet<>();
        for (User itemReviewer : itemReviewers) {
            reviewers.add(reviewerService.getByItemAndUsername(workItem.getId(), itemReviewer.getUsername()));
        }
        return reviewers;
    }

    private boolean isDraft(WorkItem workItem) {
        return workItem.getTeam() == null;
    }
}
