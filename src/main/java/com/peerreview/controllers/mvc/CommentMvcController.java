package com.peerreview.controllers.mvc;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.CommentModelMapper;
import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Comment;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.CommentDto;
import com.peerreview.services.contracts.CommentService;
import com.peerreview.services.contracts.UserService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/peerreview/{itemId}/comments")
public class CommentMvcController {

    private final CommentService commentService;
    private final WorkItemService workItemService;
    private final UserService userService;
    private final CommentModelMapper commentModelMapper;
    private final AuthenticationHelper authenticationHelper;

    public CommentMvcController(CommentService commentService, WorkItemService workItemService, UserService userService, CommentModelMapper commentModelMapper, AuthenticationHelper authenticationHelper) {
        this.commentService = commentService;
        this.workItemService = workItemService;
        this.userService = userService;
        this.commentModelMapper = commentModelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping()
    public String showCreateCommentPage(@PathVariable("itemId") int itemId, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            WorkItem workItem = workItemService.getById(itemId, user);
            model.addAttribute("commentDto", new CommentDto());
            model.addAttribute("item", workItem);
            return "create-comment";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping()
    public String createComment(@PathVariable("itemId") int itemId, @Valid @ModelAttribute CommentDto commentDto, BindingResult errors, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if (errors.hasErrors()) {
                return "redirect:/peerreview/" + itemId + "/comments";
            }
            WorkItem workItem = workItemService.getById(itemId, user);
            Comment comment = commentModelMapper.fromDto(commentDto, user, workItem);
            commentService.create(comment);

            if (isDraft(workItem)) {
                return "redirect:/peerreview/items/draft=" + workItem.getId();
            }
            return "redirect:/peerreview/" + workItem.getTeam().getId() + "/items/" + itemId;
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @ModelAttribute("user")
    public User populateUser(HttpSession session) {
        return authenticationHelper.tryGetUser(session);
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    private boolean isDraft(WorkItem workItem) {
        return workItem.getTeam() == null;
    }
}
