package com.peerreview.controllers.mvc;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.TeamModelMapper;
import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.models.Team;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.MemberDto;
import com.peerreview.models.dto.TeamDto;
import com.peerreview.models.dto.TeamSearchDto;
import com.peerreview.services.contracts.TeamService;
import com.peerreview.services.contracts.UserService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/teams")
public class TeamMvcController {

    private final TeamService teamService;
    private final TeamModelMapper teamModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final WorkItemService workItemService;
    private final UserService userService;

    @Autowired
    public TeamMvcController(TeamService teamService, TeamModelMapper teamModelMapper, AuthenticationHelper authenticationHelper, WorkItemService workItemService, UserService userService) {
        this.teamService = teamService;
        this.teamModelMapper = teamModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.workItemService = workItemService;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isUser")
    public boolean populateIsUser(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = authenticationHelper.tryGetUser(session);
            return user.isUser();
        }
        return false;
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("Work Item, ascending", "Work Item, descending"));
    }

    //    @ModelAttribute("teams")
//    public List<Team> populateTeams(HttpSession httpSession) {
//        return teamService.getTeamsOfUser(authenticationHelper.tryGetUser(httpSession));
//    }
    @ModelAttribute("user")
    public User populateUser(HttpSession session) {
        return authenticationHelper.tryGetUser(session);
    }

    @GetMapping("/team")
    public String showDashBoard() {
        return "team";
    }

    @GetMapping("{id}")
    public String showTeam(@PathVariable int id, Model model, HttpSession httpSession) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
            Team team = teamService.getById(id, user);
            model.addAttribute("team", team);

            System.out.println(team.getName());
            return "team-unique";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("teamName={teamName}")
    public String showTeam(@PathVariable String teamName, Model model, HttpSession httpSession) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
            Team team = teamService.getByName(teamName);
            model.addAttribute("team", team);

            System.out.println(team.getName());
            return "team-unique";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewTeamCreatePage(Model model, HttpSession httpSession) {

        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("team", new TeamDto());
        return "team-new";

    }

    @PostMapping("/new")
    public String createTeam(@Valid @ModelAttribute("team") TeamDto teamDto
            , BindingResult errors
            , HttpSession httpSession
            , Model model) {
        User user;
        if (errors.hasErrors()) {
            return "team-new";
        }
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";

        }
        try {
            Team team = teamModelMapper.fromDto(teamDto);
            teamService.create(team, user);
            return "redirect:/teams/allOwnerTeams";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_team", e.getMessage());
            return "team-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/allOwnerTeams")
    public String showAllTeamsOnTheGivenOwner(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            List<Team> teams = teamService.getTeamsOfUser(user);
            List<WorkItem> workItems = workItemService.filter(Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(user.getUsername()),
                    Optional.of("Recently"),
                    user);
            model.addAttribute("items", workItems);
            model.addAttribute("user", user);
            model.addAttribute("teams", teams);

            model.addAttribute("teamSearchDto", new TeamSearchDto());
            return "owner-all-team";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

    }

    @GetMapping("/allItemsByUser")
    public String showAllItems(Model model, HttpSession session) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            List<WorkItem> workItems = workItemService.filter(Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(user.getUsername()),
                    Optional.of("Recently"),
                    user);
            model.addAttribute("workingItems", workItems);

        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "owner-items";
    }

    @GetMapping("/myReviewerItems")
    public String showAllItemsByReviewer(Model model, HttpSession session) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            List<WorkItem> reviewerItems = workItemService.filter(Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(user.getUsername()),
                    user);
            model.addAttribute("reviewerItems", reviewerItems);

        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "reviewer-items";
    }

    @GetMapping("/teamMembers={teamId}")
    public String showAllTeamMembers(@PathVariable int teamId, Model model, HttpSession session) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            List<User> teamMembers = teamService.getAllTeamMembers(teamId, user);

            model.addAttribute("teamMembers", teamMembers);

            model.addAttribute("teamId",teamId);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "team-members";
    }

    @GetMapping("/update={teamId}")
    public String showNewTeamUpdatePage(@PathVariable("teamId") int teamId, Model model, HttpSession httpSession) {

        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("teamId", teamId);
        model.addAttribute("teamUpdate", new TeamDto());
        return "team-update";

    }

    @PostMapping("/update={teamId}")
    public String updateTeam(@Valid @ModelAttribute("teamUpdate") TeamDto teamDto, @PathVariable("teamId") int teamId, BindingResult errors, Model model, HttpSession session) {
        if (errors.hasErrors()) {
            return "team-update";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            Team teamToUpdate = teamModelMapper.fromDto(teamDto, teamId);
            teamService.update(teamToUpdate, user);
            return "redirect:/teams/allOwnerTeams";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_team", e.getMessage());
            return "team-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/addMember={teamId}")
    public String showAddMemberPage(@PathVariable("teamId") int teamId, Model model, HttpSession session) {


        User user = authenticationHelper.tryGetUser(session);
        Team team = teamService.getById(teamId, user);

        model.addAttribute("teamId", team.getId());
        model.addAttribute("addMember", new MemberDto());

        return "add-member";
    }

    @PostMapping("/addMember={teamId}")
    public String addMemberToTeam(@Valid @ModelAttribute("addMember") MemberDto memberDto,
                                  @PathVariable("teamId") int teamId, BindingResult errors, Model model, HttpSession session) {

        if (errors.hasErrors()) {
            return "owner-all-team";
        }

        try {
            User user = authenticationHelper.tryGetUser(session);
            User member = userService.getUserByEmail(memberDto.getEmail());

            teamService.addMember(teamId, member.getId(), user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        return "redirect:/teams/allOwnerTeams";
    }

    @GetMapping("/{teamId}/deleteMember={memberId}")
    public String showDeleteMemberPage(@PathVariable("teamId") int teamId, @PathVariable("memberId") int memberId, Model model, HttpSession session) {


        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("teamId",teamId);
            teamService.removeMember(teamId, memberId, user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        return "redirect:/teams/teamMembers=" + teamId;

    }


    @GetMapping("/delete={teamId}")
    public String showTeamDeletePage(@PathVariable("teamId") int teamId, Model model, HttpSession httpSession) {

        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        TeamDto teamDto = teamModelMapper.fromObject(teamId);
        model.addAttribute("teamDelete", teamDto);
        model.addAttribute("teamId", teamId);

        return "team-delete";

    }

    @PostMapping("/delete={teamId}")
    public String deleteTeam(@ModelAttribute("teamDelete") TeamDto teamDto
            , @PathVariable("teamId") int teamId, BindingResult errors, Model model, HttpSession session) {
        if (errors.hasErrors()) {
            return "team-delete";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            Team teamToDelete = teamService.getById(teamId, user);
            teamService.delete(teamToDelete.getId(), user);
            return "redirect:/teams/allOwnerTeams";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_team", e.getMessage());
            return "team-delete";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }


}
