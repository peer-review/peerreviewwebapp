package com.peerreview.controllers.mvc;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.UserModelMapper;
import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.InvitationDto;
import com.peerreview.models.dto.UserProfileDto;
import com.peerreview.services.contracts.UserService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final UserModelMapper userModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final WorkItemService workItemService;
    private final JavaMailSender mailSender;

    @Autowired
    public UserMvcController(UserService userService, UserModelMapper userModelMapper, AuthenticationHelper authenticationHelper, WorkItemService workItemService, JavaMailSender mailSender) {
        this.userService = userService;
        this.userModelMapper = userModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.workItemService = workItemService;
        this.mailSender = mailSender;
    }
//        @ModelAttribute("allUsers")
//    public List<User> populateUsers(HttpSession httpSession) {
//        return userService.getAll(authenticationHelper.tryGetUser(httpSession));
//    }

    @ModelAttribute("user")
    public User populateUser(HttpSession session) {
        return authenticationHelper.tryGetUser(session);
    }

    @GetMapping("/admin")
    public String showAdminPage() {

        return "admin";
    }

    @GetMapping("/allUsers")
    public String viewAllUsers(Model model, HttpSession session) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            List<User> allUsers = userService.getAll(user);


            model.addAttribute("allUsers", allUsers);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        return "all-users";
    }

   @GetMapping("/changeRole={memberId}")
    public String changeRole(@PathVariable("memberId") int memberId, Model model,HttpSession session){

        try {
            User user = authenticationHelper.tryGetUser(session);
            User member = userService.getById(memberId,user);
           userService.create(member,user);
            List<User> allUsers = userService.getAll(user);


            model.addAttribute("allUsers", allUsers);
        }catch (UnauthorizedOperationException e){
            return "redirect:/allUsers";
        }
        return "all-users";
    }
    @GetMapping("/deleteMember={memberId}")
    public String deleteMember(@PathVariable("memberId") int memberId, Model model, HttpSession session){

        try {
            User user = authenticationHelper.tryGetUser(session);

            userService.delete(memberId,user);
            List<User> allUsers = userService.getAll(user);


            model.addAttribute("allUsers", allUsers);
        }catch (UnauthorizedOperationException e){
            return "all-users";
        }
        return "redirect:/users/allUsers";
    }
    @GetMapping("/profile")
    public String showUserProfilePage(Model model, HttpSession session) {

        try {
            User user = authenticationHelper.tryGetUser(session);
//            model.addAttribute("user", user);
            List<WorkItem> reviewers = workItemService.filter(Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(user.getUsername()),
                    Optional.of("Recently"),
                    user);
            List<WorkItem> createdItems = workItemService.filter(Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(user.getUsername()),
                    Optional.of("Recently"),
                    user);
            model.addAttribute("userCount", reviewers);
            model.addAttribute("allItems", createdItems);
            UserProfileDto userProfileDto = new UserProfileDto();
            userProfileDto.setUsername(user.getUsername());
            userProfileDto.setEmail(user.getEmail());
            userProfileDto.setPhoneNumber(user.getPhoneNumber());

            model.addAttribute("profile", userProfileDto);
//            model.addAttribute("password", new UserForgotPasswordDto());

            return "profile";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

    }

    @PostMapping("/profile")
    public String updateUserProfile(@Valid @ModelAttribute("profile") UserProfileDto userProfileDto,
                                    @RequestParam(required = false, name = "file") MultipartFile multipartFile
            , BindingResult errors, HttpSession session) {


        if (errors.hasErrors()) {
            return "profile";
        }

        try {
            User userUpdate = authenticationHelper.tryGetUser(session);
            if (userUpdate.getPassword().equals(userProfileDto.getPassword())) {
                errors.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
                return "profile";
            }

            userUpdate = userModelMapper.fromProfileDto(userProfileDto
                    , authenticationHelper.tryGetUser(session).getId());

            String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
            if (!multipartFile.isEmpty()) {
                userUpdate.setPhoto(fileName);
                String uploadDirectory = "./profile-picture/" + userUpdate.getId();
                Path uploadPath = Paths.get(uploadDirectory);
                if (!Files.exists(uploadPath)) {
                    Files.createDirectories(uploadPath);
                }
                try (InputStream inputStream = multipartFile.getInputStream()) {
                    Path filePath = uploadPath.resolve(fileName);
                    Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);

                } catch (IOException e) {
                    throw new IOException("Could not save uploaded file: " + fileName);
                }
            }
            userService.update(userUpdate, userUpdate);


        } catch (DuplicateEntityException e) {
            errors.rejectValue("username", "username_error", e.getMessage());
            return "profile";
        } catch (AuthenticationFailureException e) {
            errors.rejectValue("username", "auth_error", e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/auth/login";
    }


    @GetMapping("/invite")
    public String showInvitationPage(Model model) {


        model.addAttribute("invite", new InvitationDto());

        return "invitation";
    }

    @PostMapping("/invite")
    public String invitationPage(@Valid @ModelAttribute("invite") InvitationDto invitationDto
            , BindingResult bindingResult, HttpServletRequest request, HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "invitation";
        }


        try {
            User user = authenticationHelper.tryGetUser(session);
            List<User> users = userService.getAll(user);

            userService.getUserByEmail(invitationDto.getEmail().trim());


        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            String recipient = invitationDto.getEmail();
            String subject = "Email Invitation: ";


            String message = "Please click on the link to register your account.";

            String scheme = request.getScheme();
            String serverName = request.getServerName();
            int serverPort = request.getServerPort();
            String contextPath = "/auth/register";

            String appUrl = scheme + "://" + serverName + ":" + serverPort + contextPath;

            SimpleMailMessage email = new SimpleMailMessage();
            email.setFrom(invitationDto.getEmail().trim());
            email.setTo(recipient);
            email.setSubject(subject);
            email.setText(message + appUrl);


            mailSender.send(email);
            return "redirect:/teams/team";
        }

        return "team";
    }


}
