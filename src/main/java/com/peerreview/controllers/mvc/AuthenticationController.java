package com.peerreview.controllers.mvc;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.UserModelMapper;
import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.models.OnRegistrationCompleteEvent;
import com.peerreview.models.User;
import com.peerreview.models.VerificationToken;
import com.peerreview.models.dto.UserDto;
import com.peerreview.models.dto.UserEmailDto;
import com.peerreview.models.dto.UserForgotPasswordDto;
import com.peerreview.models.dto.UserLoginDto;
import com.peerreview.services.contracts.UserService;
import com.peerreview.services.contracts.VerificationTokenService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Logger;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserService userService;
    private final UserModelMapper userModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ApplicationEventPublisher eventPublisher;
    private final MessageSource messages;
    private final VerificationTokenService verificationTokenService;
    private final WorkItemService workItemService;
    private final JavaMailSender mailSender;


    private Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    public AuthenticationController(UserService userService, UserModelMapper userModelMapper, AuthenticationHelper authenticationHelper, ApplicationEventPublisher eventPublisher, MessageSource messages, VerificationTokenService verificationTokenService, WorkItemService workItemService, JavaMailSender mailSender) {
        this.userService = userService;
        this.userModelMapper = userModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.eventPublisher = eventPublisher;
        this.messages = messages;
        this.verificationTokenService = verificationTokenService;
        this.workItemService = workItemService;
        this.mailSender = mailSender;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

//    @ModelAttribute("isUser")
//    public boolean populateIsEmployee(HttpSession session) {
//        if (session.getAttribute("currentUser") != null) {
//            User user = authenticationHelper.tryGetUser(session);
//            return user.isUser();
//        }
//        return false;
//    }


    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new UserLoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") UserLoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            User user = authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());


            if (user.isAdmin()) {
                return "redirect:/users/admin";
            }
            return "redirect:/teams/team";

        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }




    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {


        model.addAttribute("register", new UserDto());

        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("register") UserDto userDto
            , BindingResult bindingResult, WebRequest webRequest, Model model) {
        User registeredUser = new User();

        if (bindingResult.hasErrors()) {
            return "register";
        }

        try {
            registeredUser = userModelMapper.fromDto(userDto);
            userService.register(registeredUser);

            String appUrl = webRequest.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registeredUser, webRequest.getLocale(), appUrl));
//            model.addAttribute("successRegister", userDto);

        } catch (DuplicateEntityException e) {
            model.addAttribute("message", "An account for that username/email already exists.");
           bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", "There is already an account with this username: " + registeredUser.getUsername());
            return "register";
        }
        return "confirmRegistration";
    }

    @GetMapping("/confirmRegistration/{tokenId}")
    public String confirmRegistration
            (WebRequest request, Model model, @PathVariable("tokenId") String token) {

        Locale locale = request.getLocale();

        VerificationToken verificationToken = verificationTokenService.findByToken(token);
        if (verificationToken == null) {
            String message = messages.getMessage("auth.message.invalidToken", null, locale);
            model.addAttribute("message", message);
            return "redirect:/auth/access-denied";
        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            String messageValue = messages.getMessage("auth.message.expired", null, locale);
            model.addAttribute("message", messageValue);
            return "redirect:/auth/access-denied";
        }

        user.setEnabled(true);
//      return "redirect:/auth/login";
        return "registrationSuccess";
    }

    @GetMapping("/forgotPassword")
    public String showForgotPasswordPage(Model model, HttpSession session) {


        model.addAttribute("forgot", new UserEmailDto());

        return "forgot-password";
    }

    @PostMapping("/forgotPassword")
    public String processForgotPage(@Valid @ModelAttribute("forgot") UserEmailDto emailDto
            , BindingResult bindingResult, Model model, HttpServletRequest request, HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "forgot-password";
        }


        try {
            User user = userService.getUserByEmail(emailDto.getEmail());


            if (userService.getUserByEmail(emailDto.getEmail()) != null && user.getEmail().equals(emailDto.getEmail().trim())) {
                String recipient = emailDto.getEmail();
                String subject = "Forgot password: ";


                String message = "Please click on the link to assign new password.";

                String scheme = request.getScheme();
                String serverName = request.getServerName();
                int serverPort = request.getServerPort();
                String contextPath = "/auth/newPassword";

                String appUrl = scheme + "://" + serverName + ":" + serverPort + contextPath;

                SimpleMailMessage email = new SimpleMailMessage();
                email.setFrom(user.getEmail());
                email.setTo(recipient);
                email.setSubject(subject);
                email.setText(message + appUrl);


                mailSender.send(email);

            }

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", emailDto.getEmail());
        }
        return "redirect:/";
    }

    @GetMapping("/newPassword")
    public String showNewPasswordPage(Model model, HttpSession session) {


        model.addAttribute("newPassword", new UserForgotPasswordDto());

        return "new-password";
    }

    @PostMapping("/newPassword")
    public String changePassword(@Valid @ModelAttribute UserForgotPasswordDto passwordDto, Model model,
                                 HttpSession session) {
//        if (bindingResult.hasErrors()) {
//            return "login";
//        }

        try {
            User user = userService.getUserByUsername(passwordDto.getUsername());
            if (passwordDto.getNewPassword().equals(passwordDto.getConfirmPassword())) {
                User userToUpdate = userModelMapper.fromDto(passwordDto, user.getId());
                userService.update(userToUpdate, user);

            }

        } catch (EntityNotFoundException e) {
            return "not-found";
        }
        model.addAttribute("login", new UserLoginDto());
        return "login";
    }

}
