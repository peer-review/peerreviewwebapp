package com.peerreview.controllers.mvc;

import com.peerreview.config.AuthenticationHelper;
import com.peerreview.controllers.mappers.DraftModelMapper;
import com.peerreview.controllers.mappers.WorkItemModelMapper;
import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Team;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.*;
import com.peerreview.services.contracts.CommentService;
import com.peerreview.services.contracts.TeamService;
import com.peerreview.services.contracts.UserService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.File;
import java.util.*;

@Controller
@RequestMapping("/peerreview")
public class ItemMvcController {

    private final WorkItemService workItemService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final CommentService commentService;
    private final DraftModelMapper draftModelMapper;
    private final WorkItemModelMapper workItemModelMapper;
    private final TeamService teamService;
    @Value("${upload.folder}")
    String UPLOADED_FOLDER;

    public ItemMvcController(WorkItemService workItemService, UserService userService, AuthenticationHelper authenticationHelper, CommentService commentService, DraftModelMapper draftModelMapper, WorkItemModelMapper workItemModelMapper, TeamService teamService) {
        this.workItemService = workItemService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.commentService = commentService;
        this.draftModelMapper = draftModelMapper;
        this.workItemModelMapper = workItemModelMapper;
        this.teamService = teamService;
    }

    @GetMapping("/items")
    public String getAll(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("items", workItemService.getAll(user));
            return "items-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/items/user")
    public String showUserItems(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("items", workItemService.filter(
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(user.getUsername()),
                    Optional.of("Recently"),
                    user
            ));
            return "items-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }


    @PostMapping("/items/user")
    public String showUserItemsFiltered(@ModelAttribute SearchItemDto searchItemDto, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<WorkItem> workItems = workItemService.filter(
                    Optional.of(searchItemDto.getTitle()),
                    Optional.of(searchItemDto.getStatus()),
                    Optional.of(""),
                    Optional.of(searchItemDto.getCreator()),
                    Optional.of(user.getUsername()),
                    Optional.of(searchItemDto.getSortParams()),
                    user
            );
            model.addAttribute("items", workItems);
            return "items-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/items/user/forReview")
    public String showUserItemsForReview(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("items", workItemService.filter(
                    Optional.of(""),
                    Optional.of("Under Review"),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(user.getUsername()),
                    Optional.of("Recently"),
                    user
            ));
            return "items-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/items/user/pending")
    public String showUserItemsPending(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("items", workItemService.filter(
                    Optional.of(""),
                    Optional.of("Pending"),
                    Optional.of(""),
                    Optional.of(""),
                    Optional.of(user.getUsername()),
                    Optional.of("Recently"),
                    user
            ));
            return "items-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/items/drafts")
    public String showUserDrafts(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("items", workItemService.getDrafts(user));
            return "items-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/items/draft")
    public String showNewItemPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("dto", new DraftDto());
            return "draft-create";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/items/draft")
    public String createUserDraft(@ModelAttribute DraftDto draftDto, BindingResult errors, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            WorkItem workItem = draftModelMapper.fromDto(draftDto, user);
            if (errors.hasErrors()) {
                return "redirect:/peerreview/items/draft";
            }
            workItemService.create(workItem, user);
            model.addAttribute("item", workItem);
            return "redirect:/peerreview/items/draft=" + workItem.getId();
        } catch (EntityNotFoundException | DuplicateEntityException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/items/updateDraft={id}")
    public String showUpdateItemPage(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("dto", workItemService.getById(id, user));
            return "draft-create";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/items/updateDraft={id}")
    public String updateUserDraft(@PathVariable int id, @ModelAttribute DraftDto draftDto, BindingResult errors, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if (errors.hasErrors()) {
                return "redirect:/peerreview/items/updateDraft=" + id;
            }
            WorkItem workItemToUpdate = draftModelMapper.fromDto(draftDto, id, user);
            workItemService.update(workItemToUpdate, user);
            model.addAttribute("item", workItemToUpdate);
            return "redirect:/peerreview/items/draft=" + workItemToUpdate.getId();
        } catch (EntityNotFoundException | DuplicateEntityException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/items/convertDraft={id}")
    public String showChooseTeamPage(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("teams", teamService.getTeamsOfUser(user));
            model.addAttribute("teamDto", new TeamDto());
            return "item-convert";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/items/convertDraft={id}")
    public String chooseTeam(@PathVariable int id, @Valid @ModelAttribute TeamDto teamDto, BindingResult errors, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
            if (errors.hasErrors()) {
                return "redirect:/peerreview/items/convertDraft=" + id;
            }
            teamService.getByName(teamDto.getName());

            return "redirect:/peerreview/items/convert=teamName=" + teamDto.getName() + "&convertDraft=" + id;
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/items/convert=teamName={teamName}&convertDraft={id}")
    public String showConvertToItemPage(@PathVariable int id, @PathVariable String teamName, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Team team = teamService.getByName(teamName);
            Set<User> allPossibleReviewers = new HashSet<>(team.getMembers());
            allPossibleReviewers.add(team.getOwner());
            allPossibleReviewers.remove(user);

            model.addAttribute("minimumReviewers", team.getMinimumReviewers());
            model.addAttribute("teams", teamService.getTeamsOfUser(user));
            model.addAttribute("reviewers", allPossibleReviewers);
            model.addAttribute("workItemDto", workItemService.getById(id, user));
            return "item-create";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/items/convert=teamName={teamName}&convertDraft={id}")
    public String convertUserDraft(@PathVariable int id, @PathVariable String teamName, @Valid @ModelAttribute WorkItemDtoMvc workItemDtoMvc, BindingResult errors, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if (errors.hasErrors()) {
                return "redirect:/peerreview/items/convert=teamName=" + teamName + "&convertDraft=" + id;
            }
            WorkItem workItem = workItemModelMapper.fromDtoMvc(workItemDtoMvc, id, user);
            workItemService.update(workItem, user);

            Team team = teamService.getByName(teamName);
            teamService.addWorkItemToTeam(team, workItem, user);
            return "redirect:/peerreview/" + team.getId() + "/items/" + workItem.getId();
        } catch (EntityNotFoundException | DuplicateEntityException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/items/draft={id}")
    public String showUserDraft(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("item", workItemService.getById(id, user));
            model.addAttribute("comments", commentService.itemComments(id));
            model.addAttribute("files", listFiles(id));
            return "draft-page";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @ModelAttribute("searchItemDto")
    public SearchItemDto populateSearchItemDto() {
        return new SearchItemDto();
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("Recently", "Oldest", "Name ↑", "Name ↓", "Status ↑", "Status ↓"));
    }

    @ModelAttribute("commentDto")
    public CommentDto populateCommentDto() {
        return new CommentDto();
    }

    @ModelAttribute("user")
    public User populateUser(HttpSession session) {
        return authenticationHelper.tryGetUser(session);
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    private File[] listFiles(int id) {
        File dir = new File(UPLOADED_FOLDER + id + "//");
        return dir.listFiles();
    }
}
