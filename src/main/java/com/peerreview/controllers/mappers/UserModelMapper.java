package com.peerreview.controllers.mappers;

import com.peerreview.exceptions.AuthenticationFailureException;
import com.peerreview.models.User;
import com.peerreview.models.dto.UserDto;
import com.peerreview.models.dto.UserForgotPasswordDto;
import com.peerreview.models.dto.UserProfileDto;
import com.peerreview.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {

    private static final String WRONG_PASSWORD = "Wrong password";
    private final UserRepository userRepository;

    @Autowired
    public UserModelMapper(UserRepository userRepository) {
        this.userRepository = userRepository;

    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);

        return user;
    }
    public User fromDto(UserForgotPasswordDto passwordDto, int userId) {
        User user = userRepository.getById(userId);
        dtoToObject(passwordDto, user);

        return user;
    }
    private void dtoToObject(UserForgotPasswordDto passwordDto, User user) {
        user.setUsername(user.getUsername());
        user.setPassword(passwordDto.getNewPassword());

    }
    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    public User fromProfileDto(UserProfileDto userProfileDto, int id) {
        User user = userRepository.getById(id);
        profileDtoToObject(userProfileDto, user);
        return user;
    }


    private void profileDtoToObject(UserProfileDto userProfileDto, User user) {

        if (!userProfileDto.getPassword().equals("No password1*")) {
            user.setUsername(userProfileDto.getUsername());
            user.setEmail(userProfileDto.getEmail());
            user.setPhoneNumber(userProfileDto.getPhoneNumber());
            user.setPassword(userProfileDto.getPassword());
        }else {
            user.setUsername(userProfileDto.getUsername());
            user.setEmail(userProfileDto.getEmail());
            user.setPhoneNumber(userProfileDto.getPhoneNumber());
        }
    }



    private void dtoToObject(UserDto userDto, User user) {

        user.setUsername(userDto.getUsername());
        if (userDto.getPassword().equals(userDto.getConfirmPassword())) {
            user.setPassword(userDto.getPassword());
        } else {
            throw new AuthenticationFailureException(WRONG_PASSWORD);
        }

        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());

        user.setPhoto("../profile-pic.png");

    }

}
