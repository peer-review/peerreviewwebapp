package com.peerreview.controllers.mappers;

import com.peerreview.models.Comment;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.CommentDto;
import org.springframework.stereotype.Component;

@Component
public class CommentModelMapper {

    public Comment fromDto(CommentDto commentDto, User user, WorkItem workItem) {
        Comment comment = new Comment();
        dtoToObjectComment(commentDto, comment, user, workItem);
        return comment;
    }

    private void dtoToObjectComment(CommentDto commentDto, Comment comment, User user, WorkItem workItem) {
        comment.setComment(commentDto.getComment());
        comment.setWorkItem(workItem);
        comment.setCommentCreator(user);
    }
}
