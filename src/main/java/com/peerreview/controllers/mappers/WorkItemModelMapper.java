package com.peerreview.controllers.mappers;

import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.WorkItemDtoBase;
import com.peerreview.models.dto.WorkItemDtoMvc;
import com.peerreview.services.contracts.StatusService;
import com.peerreview.services.contracts.UserService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class WorkItemModelMapper {

    private final WorkItemService workItemService;
    private final UserService userService;
    private final StatusService statusService;
    private final String INITIAL_STATUS_ON_CREATE = "Pending";

    public WorkItemModelMapper(WorkItemService workItemService, UserService userService, StatusService statusService) {
        this.workItemService = workItemService;
        this.userService = userService;
        this.statusService = statusService;
    }

    public WorkItem fromDto(WorkItemDtoBase workItemDto) {
        WorkItem workItem = new WorkItem();
        dtoToObjectWorkItem(workItemDto, workItem);
        return workItem;
    }

    public WorkItem fromDto(WorkItemDtoBase workItemDto, int id, User user) {
        WorkItem workItem = workItemService.getById(id, user);
        dtoToObjectWorkItem(workItemDto, workItem);
        return workItem;
    }

    private void dtoToObjectWorkItem(WorkItemDtoBase workItemDto, WorkItem workItem) {
        workItem.setTitle(workItemDto.getTitle());
        workItem.setDescription(workItemDto.getDescription());
        workItem.setDateTime(LocalDateTime.now());
    }

    public WorkItem fromDtoMvc(WorkItemDtoMvc workItemDto, User user) {
        WorkItem workItem = new WorkItem();
        dtoToObjectWorkItem(workItemDto, workItem, user);
        return workItem;
    }

    public WorkItem fromDtoMvc(WorkItemDtoMvc workItemDto, int id, User user) {
        WorkItem workItem = workItemService.getById(id, user);
        dtoToObjectWorkItem(workItemDto, workItem, user);
        return workItem;
    }

    private User getUser(String creator) {
        return userService.getUserByUsername(creator);
    }

    private void dtoToObjectWorkItem(WorkItemDtoMvc workItemDto, WorkItem workItem, User user) {
        workItem.setTitle(workItemDto.getTitle());
        workItem.setDescription(workItemDto.getDescription());
        workItem.setCreator(user);
        workItem.setDateTime(LocalDateTime.now());
        workItem.setStatus(statusService.getByName(INITIAL_STATUS_ON_CREATE));
        Set<User> reviewers = new HashSet<>();
        for (String reviewer : workItemDto.getReviewers()) {
            reviewers.add(getUser(reviewer));
        }
        workItem.setReviewers(reviewers);
    }

    public WorkItemDtoBase toDto(WorkItem workItem) {
        WorkItemDtoBase workItemDto = new WorkItemDtoBase();
        objectToDto(workItem, workItemDto);
        return workItemDto;
    }

    private void objectToDto(WorkItem workItem, WorkItemDtoBase workItemDto) {
        workItemDto.setTitle(workItem.getTitle());
        workItemDto.setDescription(workItem.getDescription());
    }


    public WorkItem fromDtoBase(WorkItemDtoBase workItemDtoBase, WorkItem workItem) {
        workItem.setTitle(workItemDtoBase.getTitle());
        workItem.setDescription(workItemDtoBase.getDescription());
        return workItem;
    }
}
