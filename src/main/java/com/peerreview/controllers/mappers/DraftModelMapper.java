package com.peerreview.controllers.mappers;

import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.DraftDto;
import com.peerreview.services.contracts.StatusService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DraftModelMapper {

    private final String INITIAL_STATUS_ON_CREATE = "Pending";
    private final StatusService statusService;
    private final WorkItemService workItemService;

    public DraftModelMapper(StatusService statusService, WorkItemService workItemService) {
        this.statusService = statusService;
        this.workItemService = workItemService;
    }

    public WorkItem fromDto(DraftDto draftDto, User user) {
        WorkItem workItem = new WorkItem();
        dtoToObjectWorkItem(draftDto, workItem, user);
        return workItem;
    }

    public WorkItem fromDto(DraftDto draftDto, int id, User user) {
        WorkItem workItem = workItemService.getById(id, user);
        dtoToObjectWorkItem(draftDto, workItem, user);
        return workItem;
    }

    private void dtoToObjectWorkItem(DraftDto draftDto, WorkItem workItem, User user) {
        workItem.setTitle(draftDto.getTitle());
        workItem.setDescription(draftDto.getDescription());
        workItem.setCreator(user);
        workItem.setDateTime(LocalDateTime.now());
        workItem.setStatus(statusService.getByName(INITIAL_STATUS_ON_CREATE));
    }
}
