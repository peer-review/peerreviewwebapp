package com.peerreview.controllers.mappers;

import com.peerreview.models.Team;
import com.peerreview.models.dto.TeamDto;
import com.peerreview.repositories.contracts.TeamRepository;
import com.peerreview.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeamModelMapper {

    private final TeamRepository teamRepository;
    private final UserRepository userRepository;

    @Autowired
    public TeamModelMapper(TeamRepository teamRepository, UserRepository userRepository) {
        this.teamRepository = teamRepository;
        this.userRepository = userRepository;
    }

    public Team fromDto(TeamDto teamDto) {
        Team team = new Team();
        dtoToObject(teamDto, team);

        return team;
    }

    public Team fromDto(TeamDto teamDto, int id) {
        Team team = teamRepository.getById(id);
        dtoToObject(teamDto, team);
        return team;
    }

    private void dtoToObject(TeamDto teamDto, Team team) {

        team.setName(teamDto.getName());
        team.setMinimumReviewers(teamDto.getMinimumReviewer());

    }

    public TeamDto fromObject(int teamId) {
        Team team = teamRepository.getById(teamId);
        TeamDto teamDto = new TeamDto();
       objectToDto(team,teamDto);
        return teamDto;
    }
    private void objectToDto(Team team,TeamDto teamDto) {

        teamDto.setName(team.getName());



    }
}
