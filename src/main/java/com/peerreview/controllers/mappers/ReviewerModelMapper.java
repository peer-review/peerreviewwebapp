package com.peerreview.controllers.mappers;


import com.peerreview.models.Reviewer;
import com.peerreview.models.Status;
import com.peerreview.models.WorkItem;
import com.peerreview.models.dto.ReviewerDto;
import com.peerreview.models.dto.StatusDto;
import com.peerreview.services.contracts.ReviewerService;
import com.peerreview.services.contracts.StatusService;
import com.peerreview.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class ReviewerModelMapper {
    private final ReviewerService reviewerService;
    private final UserService userService;
    private final StatusService statusService;
    private final String INITIAL_STATUS_ON_CREATE = "Pending";

    public ReviewerModelMapper(ReviewerService reviewerService, UserService userService, StatusService statusService) {
        this.reviewerService = reviewerService;
        this.userService = userService;
        this.statusService = statusService;
    }

    public Reviewer fromDto(ReviewerDto reviewerDto, WorkItem workItem) {
        Reviewer reviewer = new Reviewer();
        Status status = statusService.getByName(INITIAL_STATUS_ON_CREATE);
        fromDtoToObject(reviewerDto.getUsername(), reviewer, workItem, status);
        return reviewer;
    }

    public void fromDtoToObject(String reviewerUsername, Reviewer reviewer, WorkItem workItem, Status status) {
        reviewer.setWorkItem(workItem);
        reviewer.setReviewer(userService.getUserByUsername(reviewerUsername));
        reviewer.setStatus(status);
    }

    public Reviewer fromDtoWithStatus(String username, WorkItem workItem, Status status) {
        Reviewer reviewer = reviewerService.getByItemAndUsername(workItem.getId(), username);
        fromDtoToObject(username, reviewer, workItem, status);
        return reviewer;
    }

    public Reviewer fromDtoWithStatus(String username, WorkItem workItem, String statusName) {
        Reviewer reviewer = reviewerService.getByItemAndUsername(workItem.getId(), username);
        Status status = statusService.getByName(statusName);
        fromDtoToObject(username, reviewer, workItem, status);
        return reviewer;
    }
}
