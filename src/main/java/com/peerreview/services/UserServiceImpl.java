package com.peerreview.services;

import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Role;
import com.peerreview.models.User;
import com.peerreview.repositories.contracts.RoleRepository;
import com.peerreview.repositories.contracts.UserRepository;
import com.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final String USER_ERROR_MESSAGE = "Only Admin is authorized for this operations!";
    private static final int ADMIN_ROLE = 1;
    private static final int USER_ROLE = 2;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;

    }


    @Override
    public void create(User userToChange, User user) {

        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        Role role = roleRepository.getById(ADMIN_ROLE);
        Role roleUser = roleRepository.getById(USER_ROLE);
        if (userToChange.isAdmin()){

            userToChange.getRole().remove(role);
            userToChange.getRole().add(roleUser);

        }else {

            userToChange.getRole().remove(roleUser);
            userToChange.getRole().add(role);


        }

        userRepository.update(userToChange);
    }

    @Override
    public void register(User user) {
        boolean duplicateExists = true;

        try {

            userRepository.getUserByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        Role role = roleRepository.getById(USER_ROLE);
        user.getRole().add(role);
        userRepository.create(user);

    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public List<User> search(Optional<String> searchType, User user) {

        if (!user.isAdmin() || !user.isUser()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        if (searchType.isEmpty()) {
            return userRepository.getAll();
        } else {
            return userRepository.search(searchType.get());
        }
    }

    @Override
    public User getAnonymousUserByUsername(String currentUser) {
        return userRepository.getUserByUsername(currentUser);
    }

    @Override
    public User getUserByEmail(String email) {

        return userRepository.getUserByEmail(email);
    }


    @Override
    public void update(User userUpdate, User user) {
        if (!user.isAdmin() && !user.isUser()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            User userToUpdate = userRepository.getUserByUsername(userUpdate.getUsername());

            if (userToUpdate.getId() == userUpdate.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", userUpdate.getUsername());
        }

        userRepository.update(userUpdate);

    }

    @Override
    public void delete(int id, User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        User userToDelete = getById(id, user);
        userToDelete.setDeleted(true);
        userRepository.update(userToDelete);
    }

    @Override
    public List<User> getAll(User user) {
        if (!user.isAdmin() && !user.isUser()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        return userRepository.getAll();
    }

    @Override
    public User getById(int id, User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        return userRepository.getById(id);
    }
}
