package com.peerreview.services;

import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Reviewer;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.repositories.contracts.ReviewerRepository;
import com.peerreview.services.contracts.AuditLogService;
import com.peerreview.services.contracts.ReviewerService;
import com.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewerServiceImpl implements ReviewerService {
    private static final String DENIED_ACCESS = "Denied access!";
    private final ReviewerRepository reviewerRepository;
    private final WorkItemService workItemService;
    private final AuditLogService auditLogService;

    @Autowired
    public ReviewerServiceImpl(ReviewerRepository reviewerRepository, WorkItemService workItemService, AuditLogService auditLogService) {
        this.reviewerRepository = reviewerRepository;
        this.workItemService = workItemService;
        this.auditLogService = auditLogService;
    }

    @Override
    public void create(Reviewer entity, User user) {
        if (!entity.getWorkItem().getCreator().equals(user) && user.isUser()) {
            throw new UnauthorizedOperationException(DENIED_ACCESS);
        }

        boolean duplicateExists = true;
        try {
            reviewerRepository.getByItemAndUsername(entity.getWorkItem().getId(), entity.getReviewer().getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
            auditLogService.create(String.format("Add Reviewer with username '%s' in WorkItem with id=%d", entity.getReviewer().getUsername(), entity.getWorkItem().getId()));
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Reviewer", "username", String.valueOf(entity.getReviewer().getUsername()));
        }

        reviewerRepository.create(entity);
        workItemService.updateStatus(entity.getWorkItem());
    }

    @Override
    public void update(Reviewer entity, User user) {
        if (!entity.getWorkItem().getCreator().equals(user) && !entity.getWorkItem().getReviewers().contains(user)) {
            if (user.isUser()) {
                throw new UnauthorizedOperationException(DENIED_ACCESS);
            }
        }

        boolean duplicateExists = true;
        try {
            Reviewer existingReviewer = reviewerRepository.getByItemAndUsername(entity.getWorkItem().getId(), entity.getReviewer().getUsername());
            if (existingReviewer.getReviewer().getId() == entity.getReviewer().getId()) {
                duplicateExists = false;
                if (!existingReviewer.getStatus().equals(entity.getStatus())) {
                    auditLogService.create(String.format("Reviewer with username '%s' change status from '%s' to '%s' in WorkItem with ID=%d", existingReviewer.getReviewer().getUsername(), existingReviewer.getStatus().getName(), entity.getStatus().getName(), entity.getWorkItem().getId()));
                }
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Reviewer", "username", entity.getReviewer().getUsername());
        }

        reviewerRepository.update(entity);
        workItemService.updateStatus(entity.getWorkItem());
    }

    @Override
    public void delete(int id, User user) {
        Reviewer reviewerToDelete = reviewerRepository.getById(id);
        WorkItem workItemToUpdate = reviewerToDelete.getWorkItem();

        if (!workItemToUpdate.getCreator().equals(user) && user.isUser()) {
            throw new UnauthorizedOperationException(DENIED_ACCESS);
        }
        auditLogService.create(String.format("Reviewer with username '%s' removed from WorkItem with ID=%d", reviewerToDelete.getReviewer().getUsername(), workItemToUpdate.getId()));
        reviewerRepository.delete(id);
        workItemService.updateStatus(workItemToUpdate);
    }

    @Override
    public List<Reviewer> getAll(User user) {
        return reviewerRepository.getAll();
    }

    @Override
    public Reviewer getById(int id, User user) {
        return reviewerRepository.getById(id);
    }

    @Override
    public Reviewer getByItemAndUsername(int itemId, String username) {
        return reviewerRepository.getByItemAndUsername(itemId, username);
    }
}
