package com.peerreview.services.contracts;

import com.peerreview.models.Team;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;

import java.util.List;
import java.util.Optional;


public interface TeamService extends GenericModificationService<Team>{

    Team addMember(int team, int user, User user1);


    Team removeMember(int teamId, int memberId, User user);

    List<User> getAllTeamMembers(int teamId, User user);

    void addWorkItemToTeam(Team team, WorkItem workItem, User user);


    Team getAllPendingWorkItems(Team team, User user);

    List<Team> getAllTeamsOfTheGivenOwner(User user);

    Team getByName(String teamName);

    List<Team> getTeamsOfUser(User user);
}
