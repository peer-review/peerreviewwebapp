package com.peerreview.services.contracts;

import com.peerreview.models.User;

public interface GenericModificationService<T> extends GenericService<T> {

    void create(T entity, User user);

    void update(T entity, User user);

    void delete(int id, User user);
}
