package com.peerreview.services.contracts;

import java.util.List;

public interface AuditLogService {
    List<String> getAll();

    void create(String log);
}
