package com.peerreview.services.contracts;

import com.peerreview.models.User;
import com.peerreview.models.VerificationToken;

public interface VerificationTokenService  {

    void save(User user, String token);
    VerificationToken findByToken(String token);


    void createVerificationToken(User user, String token);
}
