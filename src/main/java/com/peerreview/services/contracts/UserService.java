package com.peerreview.services.contracts;

import com.peerreview.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService extends GenericModificationService<User>{


    void register(User entity);

    User getUserByUsername(String username);

    List<User> search(Optional<String> searchType, User user);

    User getAnonymousUserByUsername(String currentUser);

    User getUserByEmail(String email);
}
