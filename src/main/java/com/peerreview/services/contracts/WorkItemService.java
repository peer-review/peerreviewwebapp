package com.peerreview.services.contracts;

import com.peerreview.models.Status;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;

import java.util.List;
import java.util.Optional;

public interface WorkItemService extends GenericModificationService<WorkItem> {
    WorkItem getByTitle(String title, User user);

    List<WorkItem> search(Optional<String> search, User user);

    List<WorkItem> filter(Optional<String> title, Optional<String> status, Optional<String> team, Optional<String> creator, Optional<String> reviewer, Optional<String> sort, User user);

    WorkItem addReviewerToItem(WorkItem workItem, String username, User user);

    Status getFinalStatusOfAllReviewers(int id);

    List<WorkItem> getDrafts(User user);

    void updateStatus(WorkItem workItem);
}
