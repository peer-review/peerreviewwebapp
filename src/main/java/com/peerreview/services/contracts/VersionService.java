package com.peerreview.services.contracts;

import com.peerreview.models.User;
import com.peerreview.models.Version;
import com.peerreview.models.WorkItem;

import java.util.List;

public interface VersionService extends GenericModificationService<Version> {
    Version getByTitle(String title);

    void createVersion(WorkItem workItem, User user);

    List<Version> getAllVersionOfItem(int id);
}
