package com.peerreview.services.contracts;

import com.peerreview.models.Status;
import com.peerreview.services.contracts.GenericService;

public interface StatusService extends GenericService<Status> {
    Status getByName(String name);
}
