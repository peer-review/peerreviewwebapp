package com.peerreview.services.contracts;

import com.peerreview.models.Comment;
import com.peerreview.models.WorkItem;

import java.util.List;

public interface CommentService {
    void addCommentToTask(WorkItem workItem, Comment comment);

    Comment getById(int id);

    void create(Comment comment);

    List<Comment> itemComments(int id);
}
