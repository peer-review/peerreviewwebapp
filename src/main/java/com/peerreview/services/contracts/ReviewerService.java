package com.peerreview.services.contracts;

import com.peerreview.models.Reviewer;

public interface ReviewerService extends GenericModificationService<Reviewer>{
    Reviewer getByItemAndUsername(int itemId, String username);
}
