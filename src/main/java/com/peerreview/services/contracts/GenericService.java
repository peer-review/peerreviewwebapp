package com.peerreview.services.contracts;

import com.peerreview.models.User;

import java.util.List;

public interface GenericService<T> {

    List<T> getAll(User user);

    T getById(int id, User user);
}
