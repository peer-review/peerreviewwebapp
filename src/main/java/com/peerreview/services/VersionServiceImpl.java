package com.peerreview.services;

import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.User;
import com.peerreview.models.Version;
import com.peerreview.models.WorkItem;
import com.peerreview.repositories.contracts.VersionRepository;
import com.peerreview.services.contracts.VersionService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class VersionServiceImpl implements VersionService {

    private static final String CREATORS_CAN_DELETE_VERSION = "Only creators can delete Version.";
    private final VersionRepository versionRepository;

    public VersionServiceImpl(VersionRepository versionRepository) {
        this.versionRepository = versionRepository;
    }

    @Override
    public void create(Version entity, User user) {
        boolean duplicateExists = true;
        try {
            versionRepository.getById(entity.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Version", "id", String.valueOf(entity.getId()));
        }

        versionRepository.create(entity);

    }

    @Override
    public void update(Version entity, User user) {
        boolean duplicateExists = true;
        try {
            Version existingVersion = versionRepository.getById(entity.getId());
            if (existingVersion.getId() == entity.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Version", "id", String.valueOf(entity.getId()));
        }

        versionRepository.update(entity);

    }

    @Override
    public void delete(int id, User user) {
        Version versionToDelete = versionRepository.getById(id);

        if (!versionToDelete.getCreator().equals(user)) {
            throw new UnauthorizedOperationException(CREATORS_CAN_DELETE_VERSION);
        }

        versionRepository.delete(id);
    }

    @Override
    public List<Version> getAll(User user) {
        return versionRepository.getAll();
    }

    @Override
    public Version getById(int id, User user) {
        return versionRepository.getById(id);
    }

    @Override
    public Version getByTitle(String title) {
        return versionRepository.getByTitle(title);
    }

    @Override
    public void createVersion(WorkItem workItem, User user) {
        Version version = new Version();

        version.setItemId(workItem.getId());
        version.setItemTitle(workItem.getTitle());
        version.setItemDescription(workItem.getDescription());
        version.setCreator(workItem.getCreator());
        version.setUpload(LocalDateTime.now());
        version.setStatus(workItem.getStatus());
        version.setLastUpdate(workItem.getDateTime());
        create(version, user);
    }

    @Override
    public List<Version> getAllVersionOfItem(int id) {
        return versionRepository.getAllVersionOfItem(id);
    }
}
