package com.peerreview.services;

import com.peerreview.models.Comment;
import com.peerreview.models.WorkItem;
import com.peerreview.repositories.contracts.CommentRepository;
import com.peerreview.services.contracts.CommentService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public void addCommentToTask(WorkItem workItem, Comment comment) {
        commentRepository.create(comment);

        workItem.addComment(comment);
    }

    @Override
    public Comment getById(int id) {
        return commentRepository.getById(id);
    }

    @Override
    public void create(Comment comment) {
        commentRepository.create(comment);
    }

    @Override
    public List<Comment> itemComments(int id) {
        return commentRepository.itemComments(id);
    }
}
