package com.peerreview.services;

import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Status;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.repositories.contracts.WorkItemRepository;
import com.peerreview.services.contracts.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class WorkItemServiceImpl implements WorkItemService {

    private static final String ONLY_ADMINS_HAS_ACCESS = "Denied access! Only admins have access.";
    private static final String ONLY_CREATOR_REVIEWERS_CHANGE = "Denied access! Only creator and reviewers can change.";
    private static final String ONLY_CREATOR_REVIEWERS_ADD = "Denied access! Only creator and reviewers can add.";
    private static final String ONLY_CREATOR_PERMISSION = "Denied access! Only creator have permission.";
    private static final String DENIED_ACCESS = "Denied access!";
    private final WorkItemRepository workItemRepository;
    private final UserService userService;
    private final StatusService statusService;
    private final VersionService versionService;
    private final AuditLogService auditLogService;
    @Value("${upload.folder}")
    private String UPLOADED_FOLDER;

    public WorkItemServiceImpl(WorkItemRepository workItemRepository, UserService userService, StatusService statusService, VersionService versionService, AuditLogService auditLogService) {
        this.workItemRepository = workItemRepository;
        this.userService = userService;
        this.statusService = statusService;
        this.versionService = versionService;
        this.auditLogService = auditLogService;
    }

    @Override
    public void create(WorkItem entity, User user) {
        boolean duplicateExists = true;
        try {
            workItemRepository.getById(entity.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("WorkItem", "id", String.valueOf(entity.getId()));
        }

        workItemRepository.create(entity);

        File createFolder = new File(UPLOADED_FOLDER + entity.getId() + "//");
        createFolder.mkdir();
    }

    @Override
    public void update(WorkItem entity, User user) {
        if (!entity.getCreator().equals(user) && !entity.getReviewers().contains(user)) {
            throw new UnauthorizedOperationException(ONLY_CREATOR_REVIEWERS_CHANGE);
        }

        boolean duplicateExists = true;
        try {
            WorkItem existingWorkItem = workItemRepository.getById(entity.getId());
            if (existingWorkItem.getId() == entity.getId()) {
                duplicateExists = false;
                if (!existingWorkItem.getTitle().equals(entity.getTitle())) {
                    auditLogService.create(String.format("Title: '%s' changed to '%s' of item with id=%d",existingWorkItem.getTitle(), entity.getTitle(), entity.getId()));
                }
                if (!existingWorkItem.getDescription().equals(entity.getDescription())) {
                    auditLogService.create(String.format("Description: '%s' changed to '%s' of item with id=%d",existingWorkItem.getDescription(), entity.getDescription(), entity.getId()));
                }
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("WorkItem", "id", String.valueOf(entity.getId()));
        }

        versionService.createVersion(entity, user);

        workItemRepository.update(entity);

    }

    @Override
    public void delete(int id, User user) {
        WorkItem workItemToDelete = workItemRepository.getById(id);

        if (!workItemToDelete.getCreator().equals(user)) {
            throw new UnauthorizedOperationException(ONLY_CREATOR_PERMISSION);
        }
        workItemToDelete.setDeleted(true);
        workItemRepository.update(workItemToDelete);
    }

    @Override
    public List<WorkItem> getAll(User user) {
        isAdmin(user);
        return workItemRepository.getAll();
    }

    @Override
    public WorkItem getById(int id, User user) {
        WorkItem workItem = workItemRepository.getById(id);

        if (!workItem.getCreator().equals(user) && !workItem.getReviewers().contains(user)) {
            throw new UnauthorizedOperationException(DENIED_ACCESS);
        }

        return workItem;
    }

    @Override
    public WorkItem getByTitle(String title, User user) {
        WorkItem workItem = workItemRepository.getByTitle(title);

        if (!workItem.getCreator().equals(user) && !workItem.getReviewers().contains(user)) {
            throw new UnauthorizedOperationException(DENIED_ACCESS);
        }

        return workItem;
    }

    @Override
    public List<WorkItem> search(Optional<String> search, User user) {
        if (search.isEmpty()) {
            return workItemRepository.getAll();
        } else {
            return workItemRepository.search(search);
        }
    }

    @Override
    public List<WorkItem> filter(Optional<String> title, Optional<String> status, Optional<String> team, Optional<String> creator, Optional<String> reviewer, Optional<String> sort, User user) {
        return workItemRepository.filter(title, status, team, creator, reviewer, sort);
    }

    @Override
    public WorkItem addReviewerToItem(WorkItem workItemToUpdate, String username, User user) {

        if (!workItemToUpdate.getCreator().equals(user)) {
            throw new UnauthorizedOperationException(ONLY_CREATOR_PERMISSION);
        }

        User reviewer = userService.getUserByUsername(username);

        workItemToUpdate.addReviewer(reviewer);

        updateStatus(workItemToUpdate);

        return workItemToUpdate;
    }

    @Override
    public Status getFinalStatusOfAllReviewers(int id) {
        List<Status> finalStatuses = workItemRepository.getAllStatusesOfItem(id);
        if (finalStatuses.isEmpty()) {
            return statusService.getByName("Pending");
        }
        Set<Status> finalStatus = new HashSet<>(finalStatuses);
        if (finalStatus.size() == 1) {
            return finalStatus.stream().findFirst().get();
        } else {
            for (Status status : finalStatus) {
                if (status.getName().equalsIgnoreCase("rejected")) {
                    return status;
                }
            }
        }

        int underReviewStatus = 0;
        Status underReview = null;
        int changeRequestedStatus = 0;
        Status changeRequest = null;
        for (Status status : finalStatus) {
            if (status.getName().equalsIgnoreCase("Under Review")) {
                underReviewStatus++;
                underReview = status;
            }
            if (status.getName().equalsIgnoreCase("change requested")) {
                changeRequestedStatus++;
                changeRequest = status;
            }
        }

        if (underReviewStatus >= changeRequestedStatus) {
            return underReview == null ? statusService.getByName("Pending") : underReview;
        } else {
            return changeRequest == null ? statusService.getByName("Pending") : changeRequest;
        }
    }

    @Override
    public List<WorkItem> getDrafts(User user) {
        return workItemRepository.getDrafts(user);
    }

    @Override
    public void updateStatus(WorkItem workItem) {
        Status status = getFinalStatusOfAllReviewers(workItem.getId());

        workItem.setStatus(status);

        workItemRepository.update(workItem);
    }

    private boolean isAdmin(User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_HAS_ACCESS);
        }
        return true;
    }
}
