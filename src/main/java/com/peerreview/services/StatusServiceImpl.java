package com.peerreview.services;

import com.peerreview.models.Status;
import com.peerreview.models.User;
import com.peerreview.repositories.contracts.StatusRepository;
import com.peerreview.services.contracts.StatusService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<Status> getAll(User user) {
        return statusRepository.getAll();
    }

    @Override
    public Status getById(int id, User user) {
        return statusRepository.getById(id);
    }

    @Override
    public Status getByName(String name) {
        return statusRepository.getByName(name);
    }
}
