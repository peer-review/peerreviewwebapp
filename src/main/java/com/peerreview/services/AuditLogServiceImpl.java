package com.peerreview.services;

import com.peerreview.repositories.contracts.AuditLogRepository;
import com.peerreview.services.contracts.AuditLogService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuditLogServiceImpl implements AuditLogService {
    private final AuditLogRepository auditLogRepository;

    public AuditLogServiceImpl(AuditLogRepository auditLogRepository) {
        this.auditLogRepository = auditLogRepository;
    }

    @Override
    public List<String> getAll() {
        return auditLogRepository.getAll();
    }

    @Override
    public void create(String log) {
        auditLogRepository.create(log);
    }
}
