package com.peerreview.services;

import com.peerreview.exceptions.DuplicateEntityException;
import com.peerreview.exceptions.EntityNotFoundException;
import com.peerreview.exceptions.UnauthorizedOperationException;
import com.peerreview.models.Team;
import com.peerreview.models.User;
import com.peerreview.models.WorkItem;
import com.peerreview.repositories.contracts.TeamRepository;
import com.peerreview.repositories.contracts.UserRepository;
import com.peerreview.services.contracts.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {
    private static final String USER_ERROR_MESSAGE = "Only Admin is authorized for this operations!";
    private static final String OWNER_ERROR_MESSAGE = "Only Owner is authorized for this operations!";

    private final TeamRepository teamRepository;
    private final UserRepository userRepository;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository, UserRepository userRepository) {
        this.teamRepository = teamRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void create(Team team, User user) {

        if (!user.isAdmin() && !user.isUser()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;

        try {
            teamRepository.getByTeamName(team.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Team", "name", team.getName());
        }


        team.setOwner(user);
        teamRepository.create(team);

    }

    @Override
    public Team getAllPendingWorkItems(Team team, User user) {

        if (!team.getOwner().equals(user) || !team.getMembers().contains(user) || !user.isAdmin()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }

        return teamRepository.getAllPendingWorkItems(team);

    }

    @Override
    public List<Team> getAllTeamsOfTheGivenOwner(User user) {
        return teamRepository.getAllOfTheGivenOwner(user);
    }

    @Override
    public Team getByName(String teamName) {
        return teamRepository.getByTeamName(teamName);
    }

    @Override
    public List<Team> getTeamsOfUser(User user) {
        return teamRepository.getTeamsOfUser(user);
    }



    @Override
    public void update(Team team, User user) {

        if (!user.isAdmin() && !team.getOwner().getUsername().equals(user.getUsername())) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            Team existingTeam = teamRepository.getById(team.getId());
            if (!existingTeam.getName().equals(team.getName())) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Team", "name", team.getName());
        }

        teamRepository.update(team);
    }

    @Override
    public void delete(int id, User user) {
        Team teamToDelete = getById(id, user);

        if (!teamToDelete.getOwner().equals(user) && !user.isUser() && !user.isAdmin()) {
            throw new UnauthorizedOperationException(OWNER_ERROR_MESSAGE);
        }

        teamToDelete.setDeleted(true);
        teamRepository.update(teamToDelete);
    }

    @Override
    public List<Team> getAll(User user) {

        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        return teamRepository.getAll();
    }

    @Override
    public Team getById(int id, User user) {

        Team team = teamRepository.getById(id);
        if (!user.isAdmin() && !team.getMembers().contains(user) && !team.getOwner().equals(user)) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        return team;
    }

    @Override
    public Team addMember(int teamId, int memberId, User user) {

        Team teamToAdd = teamRepository.getById(teamId);

        if (!teamToAdd.getOwner().equals(user) && !user.isUser() && !user.isAdmin()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }

        User member = userRepository.getById(memberId);
        teamToAdd.getMembers().add(member);

        teamRepository.update(teamToAdd);
        return teamToAdd;
    }

    @Override
    public Team removeMember(int teamId, int memberId, User user) {

        Team teamToDelete = teamRepository.getById(teamId);

        if (!teamToDelete.getOwner().equals(user)) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        User memberToDelete = teamRepository.getTeamMemberById(memberId);

        teamToDelete.getMembers().remove(memberToDelete);
        teamRepository.update(teamToDelete);

        return teamToDelete;
    }

    @Override
    public List<User> getAllTeamMembers(int teamId, User user) {

        Team team = teamRepository.getById(teamId);
        if (!team.getMembers().contains(user) && !user.isUser() && user.isAdmin()) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        return teamRepository.getAllTeamMembers(team);
    }

    @Override
    public void addWorkItemToTeam(Team team, WorkItem workItem, User user) {
        if (!team.getOwner().equals(user) && !team.getMembers().contains(user)) {
            throw new UnauthorizedOperationException(USER_ERROR_MESSAGE);
        }
        if (team.getWorkItem().contains(workItem)) {
            throw new DuplicateEntityException("Work Item", "name", String.valueOf(workItem));
        }

        team.getWorkItem().add(workItem);

        teamRepository.update(team);

    }


}
