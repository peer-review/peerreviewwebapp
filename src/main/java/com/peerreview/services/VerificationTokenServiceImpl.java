package com.peerreview.services;

import com.peerreview.models.User;
import com.peerreview.models.VerificationToken;
import com.peerreview.repositories.contracts.VerificationTokenRepository;
import com.peerreview.services.contracts.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class VerificationTokenServiceImpl implements VerificationTokenService {

    private final VerificationTokenRepository verificationTokenRepository;

    @Autowired
    public VerificationTokenServiceImpl(VerificationTokenRepository verificationTokenRepository) {
        this.verificationTokenRepository = verificationTokenRepository;
    }


    @Override
    public void save(User user, String token) {
        VerificationToken verificationToken = new VerificationToken();
        verificationTokenRepository.create(verificationToken);
    }

    @Override
    public VerificationToken findByToken(String verificationToken) {
        return verificationTokenRepository.findByToken(verificationToken);
    }

    @Override
    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token.replace("-",""),user);

        verificationTokenRepository.create(myToken);
    }
}
